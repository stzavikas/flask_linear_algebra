#############################
# Installation Instructions #
#############################
- Please ensure that you have a configured ssh client for stash, and that the ssh keys are under '~/.ssh/' folder in your host machine. The same keys will be copied and reused in your VM.
- first install vagrant version that is on the same folder
- after installation open a terminal and navigate to this directory
- open vagrant file with your text editor, replace line """ vb.memory = "4096" """ with the desired amount of memory for your VM
- finally run 'vagrant up --provision'. Your VM should start building, and in the end you should have a working ubuntu installation with XFCE.
- credentials are root/vagrant
- eclipse should be under /opt/, but there are symlinks under /root/ and /home/vagrant/ as well.
- EM-related stuff should be under /root/em_build/. Also your checked out workspace is under /root/em_build/workspace/ (project/icicle branch by default).
- In order to build from eclipse I strongly suggest to create a script that first activates the virtualenv (/root/embuild/python2_virtualenv/bin/activate) , in order for eclipse to have the environment ready. 
- No need to care about BOOST and PYTHON env variables, they are set by default in the bashrc file of the root user.


In this post we attempt to have a look at Vagrant and see how it can fit in our workflow and help us create isolated environments. 
According to HashiCorp, Vagrant is a tool that "Creates and configures lightweight, reproducible, and portable development environments". This description is pretty vague, so let me sum it up: it is a manager for the most popular virtualisation software, that automates the creation and management of virtual machines. VMWare, Virtualbox and HyperV virtual machines can be created by a single Vagrant file, so you can still continue to use the virtualisation software you have been using all along. For the purposes of the post, I am using Virtualbox, but it shouldn't really matter, any virtualization engine you have available should work. So before starting to experiment with Vagrant make sure that you have at least one of the aforementioned software.
Each virtual machine is described by a file named Vagrantfile. This file defines any steps that we want to be performed on the virtual machine automatically for us. So let's start simple: 

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"
 
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
	vb.cpus = "4"
  end
end

This file creates a virtual machine based on an Ubuntu 16.04 image, which should be retrieved automatically from the Vagrant repositories. You can have a look at the repo if you want an image that provides additional functionality out of the box, but for the purposes of this post this should be enough. Apart from downloading and installing the image, the amount of VM RAM and CPU cores are defined as well. Also bear in mind that if you are using a different virtualizer, you have to define it here (replace 'virtualbox' with 'vmware' for instance).
After defining the VM configuration file, you can start the VM using : 
vagrant up
After startup of the VM you can ssh to it using 
vagrant ssh
If you want to stop the VM from running in order to start it at a later time, type 
vagrant halt
If you want to destroy the VM to free up allocated space, run 
vagrant destroy

If you want a minimal VM that you build up on it manually, you can use it as is, but one of Vagrant's main advantages is you are able to automate and document all the system configuration you are manually doing, so I 

