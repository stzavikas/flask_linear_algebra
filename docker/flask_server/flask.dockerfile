FROM resin/rpi-raspbian
MAINTAINER lalg_website
RUN apt-get update -y
# Install some base packages
RUN apt-get install -y tar git curl nano wget dialog net-tools build-essential
# Install Python
RUN apt-get install -y python python3 python-dev python3-dev python-distribute python3-pip

# Install postgres
RUN apt-get install -y python-software-properties software-properties-common postgresql-9.4 postgresql-client-9.4 postgresql-contrib-9.4

# Install psycopg2 for postgres connection
RUN apt-get install -y python-psycopg2 libpq-dev

# Install scipy dependencies
RUN apt-get install -y libpng-dev freetype* libblas-dev liblapack-dev libatlas-base-dev gfortran

RUN git clone https://bitbucket.org/stzavikas/flask_linear_algebra.git /root/lalg_website
RUN pip3 install -r /root/lalg_website/pip_requirements.txt

# Install npm
RUN apt-get install -y npm

# Install gulp
RUN npm install --global gulp-cli

RUN cd /root/lalg_website/lalg_frontend/static/ && npm install

# Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.4`` package when it was ``apt-get installed``
USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER lalg_db WITH SUPERUSER PASSWORD 'lalg_db';" &&\
    createdb -O lalg_db lalg_db

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.4/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/9.4/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.4/main/postgresql.conf

# Add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Set the default command to run when starting the container
RUN /usr/lib/postgresql/9.4/bin/postgres -D /var/lib/postgresql/9.4/main -c config_file=/etc/postgresql/9.4/main/postgresql.conf &

ENV PYTHONPATH=/root/lalg_website
EXPOSE 5000

CMD ["python3", "/root/lalg_website/lalg_frontend/runserver.py"]

