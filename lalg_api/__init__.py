from flask import Flask
from .lu import lu_page, svd_page, cholesky_page, polar_page, qr_page, rq_page, schur_page, hessenberg_page


def register_endpoints(app):
    app.register_blueprint(lu_page, url_prefix='/lu')
    app.register_blueprint(svd_page, url_prefix='/svd')
    app.register_blueprint(cholesky_page, url_prefix='/cholesky')
    app.register_blueprint(polar_page, url_prefix='/polar')
    app.register_blueprint(qr_page, url_prefix='/qr')
    app.register_blueprint(rq_page, url_prefix='/rq')
    app.register_blueprint(schur_page, url_prefix='/schur')
    app.register_blueprint(hessenberg_page, url_prefix='/hessenberg')


app = Flask(__name__)
register_endpoints(app)
