import sympy
from lalg_api.latex.process_latex import process_sympy


def solve_equation_system(equations):
    eq_set = []
    for eq in equations:
        first_exp = None
        for exp in parse_single_equation(eq):
            if not first_exp:
                first_exp = exp
            else:
                eq_set.append(sympy.Eq(first_exp, exp))
    print(eq_set)
    result = sympy.solve(eq_set, check=False, manual=True)
    print("RESULT: " + str(result))
    if type(result) == list:
        result = result[0]
    elif type(result) == str:
        return result
    return_string = "{"
    for key, value in result.items():
        return_string += '"' + str(key) + '" : "' + str(value) + '", '
    return_string = return_string[:-2]
    return_string += "}"
    return return_string


def parse_single_equation(eq):
    # eq = convert_root_symbols(eq)
    # eq = convert_frac_symbols(eq)
    input_exps = eq.split('=')
    for exp in input_exps:
        yield process_sympy(exp)
        # yield sympy.sympify(exp)


def convert_frac_symbols(eq):
    return eq


def convert_root_symbols(exp):
    exp = ''.join(exp.split())
    return search_for_inner_group(exp)


def search_for_inner_group(exp):
    start_index, end_index = find_start_and_end_indexes(exp)
    if start_index == -1 and end_index == -1:
        # All inner groups have been replaced, return expression
        return exp
    elif start_index != -1 and end_index != -1:
        #Replace math expressions
        return_string = replace_sqrt_before_range(exp, start_index, end_index)
        return search_for_inner_group(return_string)
    else:
        #Inconsistent number of brackets
        raise Exception('Inconsistent opening and closing brackets')


def replace_sqrt_before_range(exp, start_index, end_index):
    root_index = '2'
    if exp[start_index-1] == ']':
        start_bracket = -1
        for i in range(start_index-1, -1, -1):
            if exp[i] == '[':
                start_bracket = i
                break
        root_index = exp[start_bracket+1:(start_index-1)]
    else:
        start_bracket = start_index
    if exp[(start_bracket-5):(start_bracket)] == "\\sqrt":
        replace_string = 'root((' + exp[(start_index+1):(end_index)] + '),(' + root_index + '))'
        return exp[:(start_bracket-5)] + replace_string + exp[(end_index+1):]
    else:
        return exp[:start_index] + '(' + exp[(start_index+1):end_index] + ')' + exp[(end_index+1):]


def find_start_and_end_indexes(exp):
    search_for_end = False
    start_index = -1
    end_index = -1
    for i, c in enumerate(exp):
        if c == '{':
            start_index = i
            search_for_end = True
        elif c == '}':
            if search_for_end:
                end_index = i
                break
            else:
                raise Exception('Incorrect number of opening and closing brackets.')
    return start_index, end_index


