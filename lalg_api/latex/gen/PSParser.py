# Generated from PS.g4 by ANTLR 4.5.3
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3")
        buf.write(u"\62\u0137\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4")
        buf.write(u"\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\3\3")
        buf.write(u"\3\3\3\3\3\3\3\3\3\7\3?\n\3\f\3\16\3B\13\3\3\4\3\4\3")
        buf.write(u"\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\7\6P\n\6\f\6\16")
        buf.write(u"\6S\13\6\3\7\3\7\3\7\3\7\3\7\3\7\7\7[\n\7\f\7\16\7^\13")
        buf.write(u"\7\3\b\3\b\3\b\6\bc\n\b\r\b\16\bd\5\bg\n\b\3\t\3\t\7")
        buf.write(u"\tk\n\t\f\t\16\tn\13\t\3\n\3\n\5\nr\n\n\3\13\3\13\3\13")
        buf.write(u"\3\13\3\13\3\13\5\13z\n\13\3\f\3\f\3\f\3\f\5\f\u0080")
        buf.write(u"\n\f\3\f\3\f\3\r\3\r\3\r\3\r\5\r\u0088\n\r\3\r\3\r\3")
        buf.write(u"\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16")
        buf.write(u"\u0096\n\16\3\16\5\16\u0099\n\16\7\16\u009b\n\16\f\16")
        buf.write(u"\16\16\u009e\13\16\3\17\3\17\3\17\3\17\3\17\5\17\u00a5")
        buf.write(u"\n\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00af")
        buf.write(u"\n\20\3\21\3\21\3\21\3\21\3\22\3\22\5\22\u00b7\n\22\3")
        buf.write(u"\22\3\22\5\22\u00bb\n\22\3\23\3\23\3\23\3\23\5\23\u00c1")
        buf.write(u"\n\23\3\23\5\23\u00c4\n\23\3\23\3\23\3\23\3\23\5\23\u00ca")
        buf.write(u"\n\23\3\23\3\23\3\24\3\24\3\25\3\25\5\25\u00d2\n\25\3")
        buf.write(u"\25\5\25\u00d5\n\25\3\25\5\25\u00d8\n\25\3\25\5\25\u00db")
        buf.write(u"\n\25\5\25\u00dd\n\25\3\25\3\25\3\25\3\25\3\25\3\25\3")
        buf.write(u"\25\3\25\3\25\5\25\u00e8\n\25\3\25\5\25\u00eb\n\25\3")
        buf.write(u"\25\3\25\3\25\5\25\u00f0\n\25\3\25\3\25\3\25\3\25\3\25")
        buf.write(u"\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00fe\n\25\3")
        buf.write(u"\25\3\25\3\25\3\25\3\25\3\25\5\25\u0106\n\25\3\26\3\26")
        buf.write(u"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0111\n\26\3")
        buf.write(u"\26\3\26\3\27\6\27\u0116\n\27\r\27\16\27\u0117\3\27\5")
        buf.write(u"\27\u011b\n\27\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u0123")
        buf.write(u"\n\30\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u012b\n\31\3")
        buf.write(u"\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write(u"\2\6\4\n\f\32\34\2\4\6\b\n\f\16\20\22\24\26\30\32\34")
        buf.write(u"\36 \"$&(*,.\60\62\64\2\b\3\2,\60\3\2\4\5\4\2\6\7#%\4")
        buf.write(u"\2**\62\62\3\2\24!\3\2\22\23\u0149\2\66\3\2\2\2\48\3")
        buf.write(u"\2\2\2\6C\3\2\2\2\bG\3\2\2\2\nI\3\2\2\2\fT\3\2\2\2\16")
        buf.write(u"f\3\2\2\2\20h\3\2\2\2\22q\3\2\2\2\24s\3\2\2\2\26{\3\2")
        buf.write(u"\2\2\30\u0083\3\2\2\2\32\u008b\3\2\2\2\34\u00a4\3\2\2")
        buf.write(u"\2\36\u00ae\3\2\2\2 \u00b0\3\2\2\2\"\u00ba\3\2\2\2$\u00bc")
        buf.write(u"\3\2\2\2&\u00cd\3\2\2\2(\u0105\3\2\2\2*\u0107\3\2\2\2")
        buf.write(u",\u011a\3\2\2\2.\u011c\3\2\2\2\60\u0124\3\2\2\2\62\u012c")
        buf.write(u"\3\2\2\2\64\u0131\3\2\2\2\66\67\5\4\3\2\67\3\3\2\2\2")
        buf.write(u"89\b\3\1\29:\5\b\5\2:@\3\2\2\2;<\f\4\2\2<=\t\2\2\2=?")
        buf.write(u"\5\4\3\5>;\3\2\2\2?B\3\2\2\2@>\3\2\2\2@A\3\2\2\2A\5\3")
        buf.write(u"\2\2\2B@\3\2\2\2CD\5\b\5\2DE\7,\2\2EF\5\b\5\2F\7\3\2")
        buf.write(u"\2\2GH\5\n\6\2H\t\3\2\2\2IJ\b\6\1\2JK\5\f\7\2KQ\3\2\2")
        buf.write(u"\2LM\f\4\2\2MN\t\3\2\2NP\5\n\6\5OL\3\2\2\2PS\3\2\2\2")
        buf.write(u"QO\3\2\2\2QR\3\2\2\2R\13\3\2\2\2SQ\3\2\2\2TU\b\7\1\2")
        buf.write(u"UV\5\16\b\2V\\\3\2\2\2WX\f\4\2\2XY\t\4\2\2Y[\5\f\7\5")
        buf.write(u"ZW\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]\r\3\2\2\2")
        buf.write(u"^\\\3\2\2\2_`\t\3\2\2`g\5\16\b\2ac\5\20\t\2ba\3\2\2\2")
        buf.write(u"cd\3\2\2\2db\3\2\2\2de\3\2\2\2eg\3\2\2\2f_\3\2\2\2fb")
        buf.write(u"\3\2\2\2g\17\3\2\2\2hl\5\32\16\2ik\5\22\n\2ji\3\2\2\2")
        buf.write(u"kn\3\2\2\2lj\3\2\2\2lm\3\2\2\2m\21\3\2\2\2nl\3\2\2\2")
        buf.write(u"or\7\61\2\2pr\5\24\13\2qo\3\2\2\2qp\3\2\2\2r\23\3\2\2")
        buf.write(u"\2sy\7\16\2\2tz\5\30\r\2uz\5\26\f\2vw\5\30\r\2wx\5\26")
        buf.write(u"\f\2xz\3\2\2\2yt\3\2\2\2yu\3\2\2\2yv\3\2\2\2z\25\3\2")
        buf.write(u"\2\2{|\7\'\2\2|\177\7\n\2\2}\u0080\5\b\5\2~\u0080\5\6")
        buf.write(u"\4\2\177}\3\2\2\2\177~\3\2\2\2\u0080\u0081\3\2\2\2\u0081")
        buf.write(u"\u0082\7\13\2\2\u0082\27\3\2\2\2\u0083\u0084\7(\2\2\u0084")
        buf.write(u"\u0087\7\n\2\2\u0085\u0088\5\b\5\2\u0086\u0088\5\6\4")
        buf.write(u"\2\u0087\u0085\3\2\2\2\u0087\u0086\3\2\2\2\u0088\u0089")
        buf.write(u"\3\2\2\2\u0089\u008a\7\13\2\2\u008a\31\3\2\2\2\u008b")
        buf.write(u"\u008c\b\16\1\2\u008c\u008d\5\34\17\2\u008d\u009c\3\2")
        buf.write(u"\2\2\u008e\u008f\f\4\2\2\u008f\u0095\7(\2\2\u0090\u0096")
        buf.write(u"\5\"\22\2\u0091\u0092\7\n\2\2\u0092\u0093\5\b\5\2\u0093")
        buf.write(u"\u0094\7\13\2\2\u0094\u0096\3\2\2\2\u0095\u0090\3\2\2")
        buf.write(u"\2\u0095\u0091\3\2\2\2\u0096\u0098\3\2\2\2\u0097\u0099")
        buf.write(u"\5.\30\2\u0098\u0097\3\2\2\2\u0098\u0099\3\2\2\2\u0099")
        buf.write(u"\u009b\3\2\2\2\u009a\u008e\3\2\2\2\u009b\u009e\3\2\2")
        buf.write(u"\2\u009c\u009a\3\2\2\2\u009c\u009d\3\2\2\2\u009d\33\3")
        buf.write(u"\2\2\2\u009e\u009c\3\2\2\2\u009f\u00a5\5\36\20\2\u00a0")
        buf.write(u"\u00a5\5 \21\2\u00a1\u00a5\5\"\22\2\u00a2\u00a5\5$\23")
        buf.write(u"\2\u00a3\u00a5\5(\25\2\u00a4\u009f\3\2\2\2\u00a4\u00a0")
        buf.write(u"\3\2\2\2\u00a4\u00a1\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4")
        buf.write(u"\u00a3\3\2\2\2\u00a5\35\3\2\2\2\u00a6\u00a7\7\b\2\2\u00a7")
        buf.write(u"\u00a8\5\b\5\2\u00a8\u00a9\7\t\2\2\u00a9\u00af\3\2\2")
        buf.write(u"\2\u00aa\u00ab\7\f\2\2\u00ab\u00ac\5\b\5\2\u00ac\u00ad")
        buf.write(u"\7\r\2\2\u00ad\u00af\3\2\2\2\u00ae\u00a6\3\2\2\2\u00ae")
        buf.write(u"\u00aa\3\2\2\2\u00af\37\3\2\2\2\u00b0\u00b1\7\16\2\2")
        buf.write(u"\u00b1\u00b2\5\b\5\2\u00b2\u00b3\7\16\2\2\u00b3!\3\2")
        buf.write(u"\2\2\u00b4\u00b6\t\5\2\2\u00b5\u00b7\5.\30\2\u00b6\u00b5")
        buf.write(u"\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00bb\3\2\2\2\u00b8")
        buf.write(u"\u00bb\7+\2\2\u00b9\u00bb\7)\2\2\u00ba\u00b4\3\2\2\2")
        buf.write(u"\u00ba\u00b8\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bb#\3\2\2")
        buf.write(u"\2\u00bc\u00bd\7&\2\2\u00bd\u00c3\7\n\2\2\u00be\u00c4")
        buf.write(u"\7*\2\2\u00bf\u00c1\7*\2\2\u00c0\u00bf\3\2\2\2\u00c0")
        buf.write(u"\u00c1\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c4\5\b\5")
        buf.write(u"\2\u00c3\u00be\3\2\2\2\u00c3\u00c0\3\2\2\2\u00c4\u00c5")
        buf.write(u"\3\2\2\2\u00c5\u00c6\7\13\2\2\u00c6\u00c9\7\n\2\2\u00c7")
        buf.write(u"\u00ca\7)\2\2\u00c8\u00ca\5\b\5\2\u00c9\u00c7\3\2\2\2")
        buf.write(u"\u00c9\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\u00cc")
        buf.write(u"\7\13\2\2\u00cc%\3\2\2\2\u00cd\u00ce\t\6\2\2\u00ce\'")
        buf.write(u"\3\2\2\2\u00cf\u00dc\5&\24\2\u00d0\u00d2\5.\30\2\u00d1")
        buf.write(u"\u00d0\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d4\3\2\2")
        buf.write(u"\2\u00d3\u00d5\5\60\31\2\u00d4\u00d3\3\2\2\2\u00d4\u00d5")
        buf.write(u"\3\2\2\2\u00d5\u00dd\3\2\2\2\u00d6\u00d8\5\60\31\2\u00d7")
        buf.write(u"\u00d6\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00da\3\2\2")
        buf.write(u"\2\u00d9\u00db\5.\30\2\u00da\u00d9\3\2\2\2\u00da\u00db")
        buf.write(u"\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc\u00d1\3\2\2\2\u00dc")
        buf.write(u"\u00d7\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\5,\27")
        buf.write(u"\2\u00df\u0106\3\2\2\2\u00e0\u00e7\7\21\2\2\u00e1\u00e2")
        buf.write(u"\5.\30\2\u00e2\u00e3\5\60\31\2\u00e3\u00e8\3\2\2\2\u00e4")
        buf.write(u"\u00e5\5\60\31\2\u00e5\u00e6\5.\30\2\u00e6\u00e8\3\2")
        buf.write(u"\2\2\u00e7\u00e1\3\2\2\2\u00e7\u00e4\3\2\2\2\u00e7\u00e8")
        buf.write(u"\3\2\2\2\u00e8\u00ef\3\2\2\2\u00e9\u00eb\5\n\6\2\u00ea")
        buf.write(u"\u00e9\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00ec\3\2\2")
        buf.write(u"\2\u00ec\u00f0\7)\2\2\u00ed\u00f0\5$\23\2\u00ee\u00f0")
        buf.write(u"\5\n\6\2\u00ef\u00ea\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef")
        buf.write(u"\u00ee\3\2\2\2\u00f0\u0106\3\2\2\2\u00f1\u00f2\7\"\2")
        buf.write(u"\2\u00f2\u00f3\7\n\2\2\u00f3\u00f4\5\b\5\2\u00f4\u00f5")
        buf.write(u"\7\13\2\2\u00f5\u0106\3\2\2\2\u00f6\u00fd\t\7\2\2\u00f7")
        buf.write(u"\u00f8\5\62\32\2\u00f8\u00f9\5\60\31\2\u00f9\u00fe\3")
        buf.write(u"\2\2\2\u00fa\u00fb\5\60\31\2\u00fb\u00fc\5\62\32\2\u00fc")
        buf.write(u"\u00fe\3\2\2\2\u00fd\u00f7\3\2\2\2\u00fd\u00fa\3\2\2")
        buf.write(u"\2\u00fe\u00ff\3\2\2\2\u00ff\u0100\5\f\7\2\u0100\u0106")
        buf.write(u"\3\2\2\2\u0101\u0102\7\17\2\2\u0102\u0103\5*\26\2\u0103")
        buf.write(u"\u0104\5\f\7\2\u0104\u0106\3\2\2\2\u0105\u00cf\3\2\2")
        buf.write(u"\2\u0105\u00e0\3\2\2\2\u0105\u00f1\3\2\2\2\u0105\u00f6")
        buf.write(u"\3\2\2\2\u0105\u0101\3\2\2\2\u0106)\3\2\2\2\u0107\u0108")
        buf.write(u"\7\'\2\2\u0108\u0109\7\n\2\2\u0109\u010a\t\5\2\2\u010a")
        buf.write(u"\u010b\7\20\2\2\u010b\u0110\5\b\5\2\u010c\u010d\7(\2")
        buf.write(u"\2\u010d\u010e\7\n\2\2\u010e\u010f\t\3\2\2\u010f\u0111")
        buf.write(u"\7\13\2\2\u0110\u010c\3\2\2\2\u0110\u0111\3\2\2\2\u0111")
        buf.write(u"\u0112\3\2\2\2\u0112\u0113\7\13\2\2\u0113+\3\2\2\2\u0114")
        buf.write(u"\u0116\5\"\22\2\u0115\u0114\3\2\2\2\u0116\u0117\3\2\2")
        buf.write(u"\2\u0117\u0115\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u011b")
        buf.write(u"\3\2\2\2\u0119\u011b\5\34\17\2\u011a\u0115\3\2\2\2\u011a")
        buf.write(u"\u0119\3\2\2\2\u011b-\3\2\2\2\u011c\u0122\7\'\2\2\u011d")
        buf.write(u"\u0123\5\"\22\2\u011e\u011f\7\n\2\2\u011f\u0120\5\b\5")
        buf.write(u"\2\u0120\u0121\7\13\2\2\u0121\u0123\3\2\2\2\u0122\u011d")
        buf.write(u"\3\2\2\2\u0122\u011e\3\2\2\2\u0123/\3\2\2\2\u0124\u012a")
        buf.write(u"\7(\2\2\u0125\u012b\5\"\22\2\u0126\u0127\7\n\2\2\u0127")
        buf.write(u"\u0128\5\b\5\2\u0128\u0129\7\13\2\2\u0129\u012b\3\2\2")
        buf.write(u"\2\u012a\u0125\3\2\2\2\u012a\u0126\3\2\2\2\u012b\61\3")
        buf.write(u"\2\2\2\u012c\u012d\7\'\2\2\u012d\u012e\7\n\2\2\u012e")
        buf.write(u"\u012f\5\6\4\2\u012f\u0130\7\13\2\2\u0130\63\3\2\2\2")
        buf.write(u"\u0131\u0132\7\'\2\2\u0132\u0133\7\n\2\2\u0133\u0134")
        buf.write(u"\5\6\4\2\u0134\u0135\7\13\2\2\u0135\65\3\2\2\2%@Q\\d")
        buf.write(u"flqy\177\u0087\u0095\u0098\u009c\u00a4\u00ae\u00b6\u00ba")
        buf.write(u"\u00c0\u00c3\u00c9\u00d1\u00d4\u00d7\u00da\u00dc\u00e7")
        buf.write(u"\u00ea\u00ef\u00fd\u0105\u0110\u0117\u011a\u0122\u012a")
        return buf.getvalue()


class PSParser ( Parser ):

    grammarFileName = "PS.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"'+'", u"'-'", u"'*'", 
                     u"'/'", u"'('", u"')'", u"'{'", u"'}'", u"'['", u"']'", 
                     u"'|'", u"'\\lim'", u"<INVALID>", u"'\\int'", u"'\\sum'", 
                     u"'\\prod'", u"'\\log'", u"'\\ln'", u"'\\sin'", u"'\\cos'", 
                     u"'\\tan'", u"'\\csc'", u"'\\sec'", u"'\\cot'", u"'\\arcsin'", 
                     u"'\\arccos'", u"'\\arctan'", u"'\\arccsc'", u"'\\arcsec'", 
                     u"'\\arccot'", u"'\\sqrt'", u"'\\times'", u"'\\cdot'", 
                     u"'\\div'", u"'\\frac'", u"'_'", u"'^'", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"'='", u"'<'", u"'\\leq'", 
                     u"'>'", u"'\\geq'", u"'!'" ]

    symbolicNames = [ u"<INVALID>", u"WS", u"ADD", u"SUB", u"MUL", u"DIV", 
                      u"L_PAREN", u"R_PAREN", u"L_BRACE", u"R_BRACE", u"L_BRACKET", 
                      u"R_BRACKET", u"BAR", u"FUNC_LIM", u"LIM_APPROACH_SYM", 
                      u"FUNC_INT", u"FUNC_SUM", u"FUNC_PROD", u"FUNC_LOG", 
                      u"FUNC_LN", u"FUNC_SIN", u"FUNC_COS", u"FUNC_TAN", 
                      u"FUNC_CSC", u"FUNC_SEC", u"FUNC_COT", u"FUNC_ARCSIN", 
                      u"FUNC_ARCCOS", u"FUNC_ARCTAN", u"FUNC_ARCCSC", u"FUNC_ARCSEC", 
                      u"FUNC_ARCCOT", u"FUNC_SQRT", u"CMD_TIMES", u"CMD_CDOT", 
                      u"CMD_DIV", u"CMD_FRAC", u"UNDERSCORE", u"CARET", 
                      u"DIFFERENTIAL", u"LETTER", u"NUMBER", u"EQUAL", u"LT", 
                      u"LTE", u"GT", u"GTE", u"BANG", u"SYMBOL" ]

    RULE_math = 0
    RULE_relation = 1
    RULE_equality = 2
    RULE_expr = 3
    RULE_additive = 4
    RULE_mp = 5
    RULE_unary = 6
    RULE_postfix = 7
    RULE_postfix_op = 8
    RULE_eval_at = 9
    RULE_eval_at_sub = 10
    RULE_eval_at_sup = 11
    RULE_exp = 12
    RULE_comp = 13
    RULE_group = 14
    RULE_abs_group = 15
    RULE_atom = 16
    RULE_frac = 17
    RULE_func_normal = 18
    RULE_func = 19
    RULE_limit_sub = 20
    RULE_func_arg = 21
    RULE_subexpr = 22
    RULE_supexpr = 23
    RULE_subeq = 24
    RULE_supeq = 25

    ruleNames =  [ u"math", u"relation", u"equality", u"expr", u"additive", 
                   u"mp", u"unary", u"postfix", u"postfix_op", u"eval_at", 
                   u"eval_at_sub", u"eval_at_sup", u"exp", u"comp", u"group", 
                   u"abs_group", u"atom", u"frac", u"func_normal", u"func", 
                   u"limit_sub", u"func_arg", u"subexpr", u"supexpr", u"subeq", 
                   u"supeq" ]

    EOF = Token.EOF
    WS=1
    ADD=2
    SUB=3
    MUL=4
    DIV=5
    L_PAREN=6
    R_PAREN=7
    L_BRACE=8
    R_BRACE=9
    L_BRACKET=10
    R_BRACKET=11
    BAR=12
    FUNC_LIM=13
    LIM_APPROACH_SYM=14
    FUNC_INT=15
    FUNC_SUM=16
    FUNC_PROD=17
    FUNC_LOG=18
    FUNC_LN=19
    FUNC_SIN=20
    FUNC_COS=21
    FUNC_TAN=22
    FUNC_CSC=23
    FUNC_SEC=24
    FUNC_COT=25
    FUNC_ARCSIN=26
    FUNC_ARCCOS=27
    FUNC_ARCTAN=28
    FUNC_ARCCSC=29
    FUNC_ARCSEC=30
    FUNC_ARCCOT=31
    FUNC_SQRT=32
    CMD_TIMES=33
    CMD_CDOT=34
    CMD_DIV=35
    CMD_FRAC=36
    UNDERSCORE=37
    CARET=38
    DIFFERENTIAL=39
    LETTER=40
    NUMBER=41
    EQUAL=42
    LT=43
    LTE=44
    GT=45
    GTE=46
    BANG=47
    SYMBOL=48

    def __init__(self, input):
        super(PSParser, self).__init__(input)
        self.checkVersion("4.5.3")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class MathContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.MathContext, self).__init__(parent, invokingState)
            self.parser = parser

        def relation(self):
            return self.getTypedRuleContext(PSParser.RelationContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_math

        def enterRule(self, listener):
            if hasattr(listener, "enterMath"):
                listener.enterMath(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMath"):
                listener.exitMath(self)




    def math(self):

        localctx = PSParser.MathContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_math)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.relation(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RelationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.RelationContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def relation(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.RelationContext)
            else:
                return self.getTypedRuleContext(PSParser.RelationContext,i)


        def EQUAL(self):
            return self.getToken(PSParser.EQUAL, 0)

        def LT(self):
            return self.getToken(PSParser.LT, 0)

        def LTE(self):
            return self.getToken(PSParser.LTE, 0)

        def GT(self):
            return self.getToken(PSParser.GT, 0)

        def GTE(self):
            return self.getToken(PSParser.GTE, 0)

        def getRuleIndex(self):
            return PSParser.RULE_relation

        def enterRule(self, listener):
            if hasattr(listener, "enterRelation"):
                listener.enterRelation(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitRelation"):
                listener.exitRelation(self)



    def relation(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = PSParser.RelationContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_relation, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 55
            self.expr()
            self._ctx.stop = self._input.LT(-1)
            self.state = 62
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,0,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = PSParser.RelationContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_relation)
                    self.state = 57
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 58
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PSParser.EQUAL) | (1 << PSParser.LT) | (1 << PSParser.LTE) | (1 << PSParser.GT) | (1 << PSParser.GTE))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 59
                    self.relation(3) 
                self.state = 64
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,0,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class EqualityContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.EqualityContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.ExprContext)
            else:
                return self.getTypedRuleContext(PSParser.ExprContext,i)


        def EQUAL(self):
            return self.getToken(PSParser.EQUAL, 0)

        def getRuleIndex(self):
            return PSParser.RULE_equality

        def enterRule(self, listener):
            if hasattr(listener, "enterEquality"):
                listener.enterEquality(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitEquality"):
                listener.exitEquality(self)




    def equality(self):

        localctx = PSParser.EqualityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_equality)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            self.expr()
            self.state = 66
            self.match(PSParser.EQUAL)
            self.state = 67
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.ExprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def additive(self):
            return self.getTypedRuleContext(PSParser.AdditiveContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterExpr"):
                listener.enterExpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitExpr"):
                listener.exitExpr(self)




    def expr(self):

        localctx = PSParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.additive(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AdditiveContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.AdditiveContext, self).__init__(parent, invokingState)
            self.parser = parser

        def mp(self):
            return self.getTypedRuleContext(PSParser.MpContext,0)


        def additive(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.AdditiveContext)
            else:
                return self.getTypedRuleContext(PSParser.AdditiveContext,i)


        def ADD(self):
            return self.getToken(PSParser.ADD, 0)

        def SUB(self):
            return self.getToken(PSParser.SUB, 0)

        def getRuleIndex(self):
            return PSParser.RULE_additive

        def enterRule(self, listener):
            if hasattr(listener, "enterAdditive"):
                listener.enterAdditive(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAdditive"):
                listener.exitAdditive(self)



    def additive(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = PSParser.AdditiveContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 8
        self.enterRecursionRule(localctx, 8, self.RULE_additive, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self.mp(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 79
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = PSParser.AdditiveContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_additive)
                    self.state = 74
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 75
                    _la = self._input.LA(1)
                    if not(_la==PSParser.ADD or _la==PSParser.SUB):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 76
                    self.additive(3) 
                self.state = 81
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class MpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.MpContext, self).__init__(parent, invokingState)
            self.parser = parser

        def unary(self):
            return self.getTypedRuleContext(PSParser.UnaryContext,0)


        def mp(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.MpContext)
            else:
                return self.getTypedRuleContext(PSParser.MpContext,i)


        def MUL(self):
            return self.getToken(PSParser.MUL, 0)

        def CMD_TIMES(self):
            return self.getToken(PSParser.CMD_TIMES, 0)

        def CMD_CDOT(self):
            return self.getToken(PSParser.CMD_CDOT, 0)

        def DIV(self):
            return self.getToken(PSParser.DIV, 0)

        def CMD_DIV(self):
            return self.getToken(PSParser.CMD_DIV, 0)

        def getRuleIndex(self):
            return PSParser.RULE_mp

        def enterRule(self, listener):
            if hasattr(listener, "enterMp"):
                listener.enterMp(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMp"):
                listener.exitMp(self)



    def mp(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = PSParser.MpContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_mp, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.unary()
            self._ctx.stop = self._input.LT(-1)
            self.state = 90
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = PSParser.MpContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_mp)
                    self.state = 85
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 86
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PSParser.MUL) | (1 << PSParser.DIV) | (1 << PSParser.CMD_TIMES) | (1 << PSParser.CMD_CDOT) | (1 << PSParser.CMD_DIV))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 87
                    self.mp(3) 
                self.state = 92
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class UnaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.UnaryContext, self).__init__(parent, invokingState)
            self.parser = parser

        def unary(self):
            return self.getTypedRuleContext(PSParser.UnaryContext,0)


        def ADD(self):
            return self.getToken(PSParser.ADD, 0)

        def SUB(self):
            return self.getToken(PSParser.SUB, 0)

        def postfix(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.PostfixContext)
            else:
                return self.getTypedRuleContext(PSParser.PostfixContext,i)


        def getRuleIndex(self):
            return PSParser.RULE_unary

        def enterRule(self, listener):
            if hasattr(listener, "enterUnary"):
                listener.enterUnary(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnary"):
                listener.exitUnary(self)




    def unary(self):

        localctx = PSParser.UnaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_unary)
        self._la = 0 # Token type
        try:
            self.state = 100
            token = self._input.LA(1)
            if token in [PSParser.ADD, PSParser.SUB]:
                self.enterOuterAlt(localctx, 1)
                self.state = 93
                _la = self._input.LA(1)
                if not(_la==PSParser.ADD or _la==PSParser.SUB):
                    self._errHandler.recoverInline(self)
                else:
                    self.consume()
                self.state = 94
                self.unary()

            elif token in [PSParser.L_PAREN, PSParser.L_BRACKET, PSParser.BAR, PSParser.FUNC_LIM, PSParser.FUNC_INT, PSParser.FUNC_SUM, PSParser.FUNC_PROD, PSParser.FUNC_LOG, PSParser.FUNC_LN, PSParser.FUNC_SIN, PSParser.FUNC_COS, PSParser.FUNC_TAN, PSParser.FUNC_CSC, PSParser.FUNC_SEC, PSParser.FUNC_COT, PSParser.FUNC_ARCSIN, PSParser.FUNC_ARCCOS, PSParser.FUNC_ARCTAN, PSParser.FUNC_ARCCSC, PSParser.FUNC_ARCSEC, PSParser.FUNC_ARCCOT, PSParser.FUNC_SQRT, PSParser.CMD_FRAC, PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 96 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 95
                        self.postfix()

                    else:
                        raise NoViableAltException(self)
                    self.state = 98 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,3,self._ctx)


            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PostfixContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.PostfixContext, self).__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(PSParser.ExpContext,0)


        def postfix_op(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.Postfix_opContext)
            else:
                return self.getTypedRuleContext(PSParser.Postfix_opContext,i)


        def getRuleIndex(self):
            return PSParser.RULE_postfix

        def enterRule(self, listener):
            if hasattr(listener, "enterPostfix"):
                listener.enterPostfix(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitPostfix"):
                listener.exitPostfix(self)




    def postfix(self):

        localctx = PSParser.PostfixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_postfix)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self.exp(0)
            self.state = 106
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 103
                    self.postfix_op() 
                self.state = 108
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Postfix_opContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Postfix_opContext, self).__init__(parent, invokingState)
            self.parser = parser

        def BANG(self):
            return self.getToken(PSParser.BANG, 0)

        def eval_at(self):
            return self.getTypedRuleContext(PSParser.Eval_atContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_postfix_op

        def enterRule(self, listener):
            if hasattr(listener, "enterPostfix_op"):
                listener.enterPostfix_op(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitPostfix_op"):
                listener.exitPostfix_op(self)




    def postfix_op(self):

        localctx = PSParser.Postfix_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_postfix_op)
        try:
            self.state = 111
            token = self._input.LA(1)
            if token in [PSParser.BANG]:
                self.enterOuterAlt(localctx, 1)
                self.state = 109
                self.match(PSParser.BANG)

            elif token in [PSParser.BAR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 110
                self.eval_at()

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Eval_atContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Eval_atContext, self).__init__(parent, invokingState)
            self.parser = parser

        def BAR(self):
            return self.getToken(PSParser.BAR, 0)

        def eval_at_sup(self):
            return self.getTypedRuleContext(PSParser.Eval_at_supContext,0)


        def eval_at_sub(self):
            return self.getTypedRuleContext(PSParser.Eval_at_subContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_eval_at

        def enterRule(self, listener):
            if hasattr(listener, "enterEval_at"):
                listener.enterEval_at(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitEval_at"):
                listener.exitEval_at(self)




    def eval_at(self):

        localctx = PSParser.Eval_atContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_eval_at)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self.match(PSParser.BAR)
            self.state = 119
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.state = 114
                self.eval_at_sup()
                pass

            elif la_ == 2:
                self.state = 115
                self.eval_at_sub()
                pass

            elif la_ == 3:
                self.state = 116
                self.eval_at_sup()
                self.state = 117
                self.eval_at_sub()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Eval_at_subContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Eval_at_subContext, self).__init__(parent, invokingState)
            self.parser = parser

        def UNDERSCORE(self):
            return self.getToken(PSParser.UNDERSCORE, 0)

        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def equality(self):
            return self.getTypedRuleContext(PSParser.EqualityContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_eval_at_sub

        def enterRule(self, listener):
            if hasattr(listener, "enterEval_at_sub"):
                listener.enterEval_at_sub(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitEval_at_sub"):
                listener.exitEval_at_sub(self)




    def eval_at_sub(self):

        localctx = PSParser.Eval_at_subContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_eval_at_sub)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 121
            self.match(PSParser.UNDERSCORE)
            self.state = 122
            self.match(PSParser.L_BRACE)
            self.state = 125
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.state = 123
                self.expr()
                pass

            elif la_ == 2:
                self.state = 124
                self.equality()
                pass


            self.state = 127
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Eval_at_supContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Eval_at_supContext, self).__init__(parent, invokingState)
            self.parser = parser

        def CARET(self):
            return self.getToken(PSParser.CARET, 0)

        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def equality(self):
            return self.getTypedRuleContext(PSParser.EqualityContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_eval_at_sup

        def enterRule(self, listener):
            if hasattr(listener, "enterEval_at_sup"):
                listener.enterEval_at_sup(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitEval_at_sup"):
                listener.exitEval_at_sup(self)




    def eval_at_sup(self):

        localctx = PSParser.Eval_at_supContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_eval_at_sup)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 129
            self.match(PSParser.CARET)
            self.state = 130
            self.match(PSParser.L_BRACE)
            self.state = 133
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.state = 131
                self.expr()
                pass

            elif la_ == 2:
                self.state = 132
                self.equality()
                pass


            self.state = 135
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.ExpContext, self).__init__(parent, invokingState)
            self.parser = parser

        def comp(self):
            return self.getTypedRuleContext(PSParser.CompContext,0)


        def exp(self):
            return self.getTypedRuleContext(PSParser.ExpContext,0)


        def CARET(self):
            return self.getToken(PSParser.CARET, 0)

        def atom(self):
            return self.getTypedRuleContext(PSParser.AtomContext,0)


        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def subexpr(self):
            return self.getTypedRuleContext(PSParser.SubexprContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_exp

        def enterRule(self, listener):
            if hasattr(listener, "enterExp"):
                listener.enterExp(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitExp"):
                listener.exitExp(self)



    def exp(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = PSParser.ExpContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 24
        self.enterRecursionRule(localctx, 24, self.RULE_exp, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 138
            self.comp()
            self._ctx.stop = self._input.LT(-1)
            self.state = 154
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = PSParser.ExpContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp)
                    self.state = 140
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 141
                    self.match(PSParser.CARET)
                    self.state = 147
                    token = self._input.LA(1)
                    if token in [PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                        self.state = 142
                        self.atom()

                    elif token in [PSParser.L_BRACE]:
                        self.state = 143
                        self.match(PSParser.L_BRACE)
                        self.state = 144
                        self.expr()
                        self.state = 145
                        self.match(PSParser.R_BRACE)

                    else:
                        raise NoViableAltException(self)

                    self.state = 150
                    self._errHandler.sync(self);
                    la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
                    if la_ == 1:
                        self.state = 149
                        self.subexpr()

             
                self.state = 156
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class CompContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.CompContext, self).__init__(parent, invokingState)
            self.parser = parser

        def group(self):
            return self.getTypedRuleContext(PSParser.GroupContext,0)


        def abs_group(self):
            return self.getTypedRuleContext(PSParser.Abs_groupContext,0)


        def atom(self):
            return self.getTypedRuleContext(PSParser.AtomContext,0)


        def frac(self):
            return self.getTypedRuleContext(PSParser.FracContext,0)


        def func(self):
            return self.getTypedRuleContext(PSParser.FuncContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_comp

        def enterRule(self, listener):
            if hasattr(listener, "enterComp"):
                listener.enterComp(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitComp"):
                listener.exitComp(self)




    def comp(self):

        localctx = PSParser.CompContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_comp)
        try:
            self.state = 162
            token = self._input.LA(1)
            if token in [PSParser.L_PAREN, PSParser.L_BRACKET]:
                self.enterOuterAlt(localctx, 1)
                self.state = 157
                self.group()

            elif token in [PSParser.BAR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 158
                self.abs_group()

            elif token in [PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                self.enterOuterAlt(localctx, 3)
                self.state = 159
                self.atom()

            elif token in [PSParser.CMD_FRAC]:
                self.enterOuterAlt(localctx, 4)
                self.state = 160
                self.frac()

            elif token in [PSParser.FUNC_LIM, PSParser.FUNC_INT, PSParser.FUNC_SUM, PSParser.FUNC_PROD, PSParser.FUNC_LOG, PSParser.FUNC_LN, PSParser.FUNC_SIN, PSParser.FUNC_COS, PSParser.FUNC_TAN, PSParser.FUNC_CSC, PSParser.FUNC_SEC, PSParser.FUNC_COT, PSParser.FUNC_ARCSIN, PSParser.FUNC_ARCCOS, PSParser.FUNC_ARCTAN, PSParser.FUNC_ARCCSC, PSParser.FUNC_ARCSEC, PSParser.FUNC_ARCCOT, PSParser.FUNC_SQRT]:
                self.enterOuterAlt(localctx, 5)
                self.state = 161
                self.func()

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class GroupContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.GroupContext, self).__init__(parent, invokingState)
            self.parser = parser

        def L_PAREN(self):
            return self.getToken(PSParser.L_PAREN, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_PAREN(self):
            return self.getToken(PSParser.R_PAREN, 0)

        def L_BRACKET(self):
            return self.getToken(PSParser.L_BRACKET, 0)

        def R_BRACKET(self):
            return self.getToken(PSParser.R_BRACKET, 0)

        def getRuleIndex(self):
            return PSParser.RULE_group

        def enterRule(self, listener):
            if hasattr(listener, "enterGroup"):
                listener.enterGroup(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitGroup"):
                listener.exitGroup(self)




    def group(self):

        localctx = PSParser.GroupContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_group)
        try:
            self.state = 172
            token = self._input.LA(1)
            if token in [PSParser.L_PAREN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 164
                self.match(PSParser.L_PAREN)
                self.state = 165
                self.expr()
                self.state = 166
                self.match(PSParser.R_PAREN)

            elif token in [PSParser.L_BRACKET]:
                self.enterOuterAlt(localctx, 2)
                self.state = 168
                self.match(PSParser.L_BRACKET)
                self.state = 169
                self.expr()
                self.state = 170
                self.match(PSParser.R_BRACKET)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Abs_groupContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Abs_groupContext, self).__init__(parent, invokingState)
            self.parser = parser

        def BAR(self, i=None):
            if i is None:
                return self.getTokens(PSParser.BAR)
            else:
                return self.getToken(PSParser.BAR, i)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_abs_group

        def enterRule(self, listener):
            if hasattr(listener, "enterAbs_group"):
                listener.enterAbs_group(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAbs_group"):
                listener.exitAbs_group(self)




    def abs_group(self):

        localctx = PSParser.Abs_groupContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_abs_group)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(PSParser.BAR)
            self.state = 175
            self.expr()
            self.state = 176
            self.match(PSParser.BAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.AtomContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LETTER(self):
            return self.getToken(PSParser.LETTER, 0)

        def SYMBOL(self):
            return self.getToken(PSParser.SYMBOL, 0)

        def subexpr(self):
            return self.getTypedRuleContext(PSParser.SubexprContext,0)


        def NUMBER(self):
            return self.getToken(PSParser.NUMBER, 0)

        def DIFFERENTIAL(self):
            return self.getToken(PSParser.DIFFERENTIAL, 0)

        def getRuleIndex(self):
            return PSParser.RULE_atom

        def enterRule(self, listener):
            if hasattr(listener, "enterAtom"):
                listener.enterAtom(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAtom"):
                listener.exitAtom(self)




    def atom(self):

        localctx = PSParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_atom)
        self._la = 0 # Token type
        try:
            self.state = 184
            token = self._input.LA(1)
            if token in [PSParser.LETTER, PSParser.SYMBOL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 178
                _la = self._input.LA(1)
                if not(_la==PSParser.LETTER or _la==PSParser.SYMBOL):
                    self._errHandler.recoverInline(self)
                else:
                    self.consume()
                self.state = 180
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                if la_ == 1:
                    self.state = 179
                    self.subexpr()



            elif token in [PSParser.NUMBER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 182
                self.match(PSParser.NUMBER)

            elif token in [PSParser.DIFFERENTIAL]:
                self.enterOuterAlt(localctx, 3)
                self.state = 183
                self.match(PSParser.DIFFERENTIAL)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FracContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.FracContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.letter1 = None # Token
            self.upper = None # ExprContext
            self.lower = None # ExprContext

        def CMD_FRAC(self):
            return self.getToken(PSParser.CMD_FRAC, 0)

        def L_BRACE(self, i=None):
            if i is None:
                return self.getTokens(PSParser.L_BRACE)
            else:
                return self.getToken(PSParser.L_BRACE, i)

        def R_BRACE(self, i=None):
            if i is None:
                return self.getTokens(PSParser.R_BRACE)
            else:
                return self.getToken(PSParser.R_BRACE, i)

        def DIFFERENTIAL(self):
            return self.getToken(PSParser.DIFFERENTIAL, 0)

        def LETTER(self):
            return self.getToken(PSParser.LETTER, 0)

        def expr(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.ExprContext)
            else:
                return self.getTypedRuleContext(PSParser.ExprContext,i)


        def getRuleIndex(self):
            return PSParser.RULE_frac

        def enterRule(self, listener):
            if hasattr(listener, "enterFrac"):
                listener.enterFrac(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFrac"):
                listener.exitFrac(self)




    def frac(self):

        localctx = PSParser.FracContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_frac)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 186
            self.match(PSParser.CMD_FRAC)
            self.state = 187
            self.match(PSParser.L_BRACE)
            self.state = 193
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 188
                localctx.letter1 = self.match(PSParser.LETTER)
                pass

            elif la_ == 2:
                self.state = 190
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
                if la_ == 1:
                    self.state = 189
                    localctx.letter1 = self.match(PSParser.LETTER)


                self.state = 192
                localctx.upper = self.expr()
                pass


            self.state = 195
            self.match(PSParser.R_BRACE)
            self.state = 196
            self.match(PSParser.L_BRACE)
            self.state = 199
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.state = 197
                self.match(PSParser.DIFFERENTIAL)
                pass

            elif la_ == 2:
                self.state = 198
                localctx.lower = self.expr()
                pass


            self.state = 201
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Func_normalContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Func_normalContext, self).__init__(parent, invokingState)
            self.parser = parser

        def FUNC_LOG(self):
            return self.getToken(PSParser.FUNC_LOG, 0)

        def FUNC_LN(self):
            return self.getToken(PSParser.FUNC_LN, 0)

        def FUNC_SIN(self):
            return self.getToken(PSParser.FUNC_SIN, 0)

        def FUNC_COS(self):
            return self.getToken(PSParser.FUNC_COS, 0)

        def FUNC_TAN(self):
            return self.getToken(PSParser.FUNC_TAN, 0)

        def FUNC_CSC(self):
            return self.getToken(PSParser.FUNC_CSC, 0)

        def FUNC_SEC(self):
            return self.getToken(PSParser.FUNC_SEC, 0)

        def FUNC_COT(self):
            return self.getToken(PSParser.FUNC_COT, 0)

        def FUNC_ARCSIN(self):
            return self.getToken(PSParser.FUNC_ARCSIN, 0)

        def FUNC_ARCCOS(self):
            return self.getToken(PSParser.FUNC_ARCCOS, 0)

        def FUNC_ARCTAN(self):
            return self.getToken(PSParser.FUNC_ARCTAN, 0)

        def FUNC_ARCCSC(self):
            return self.getToken(PSParser.FUNC_ARCCSC, 0)

        def FUNC_ARCSEC(self):
            return self.getToken(PSParser.FUNC_ARCSEC, 0)

        def FUNC_ARCCOT(self):
            return self.getToken(PSParser.FUNC_ARCCOT, 0)

        def getRuleIndex(self):
            return PSParser.RULE_func_normal

        def enterRule(self, listener):
            if hasattr(listener, "enterFunc_normal"):
                listener.enterFunc_normal(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunc_normal"):
                listener.exitFunc_normal(self)




    def func_normal(self):

        localctx = PSParser.Func_normalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_func_normal)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 203
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PSParser.FUNC_LOG) | (1 << PSParser.FUNC_LN) | (1 << PSParser.FUNC_SIN) | (1 << PSParser.FUNC_COS) | (1 << PSParser.FUNC_TAN) | (1 << PSParser.FUNC_CSC) | (1 << PSParser.FUNC_SEC) | (1 << PSParser.FUNC_COT) | (1 << PSParser.FUNC_ARCSIN) | (1 << PSParser.FUNC_ARCCOS) | (1 << PSParser.FUNC_ARCTAN) | (1 << PSParser.FUNC_ARCCSC) | (1 << PSParser.FUNC_ARCSEC) | (1 << PSParser.FUNC_ARCCOT))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.FuncContext, self).__init__(parent, invokingState)
            self.parser = parser

        def func_normal(self):
            return self.getTypedRuleContext(PSParser.Func_normalContext,0)


        def func_arg(self):
            return self.getTypedRuleContext(PSParser.Func_argContext,0)


        def subexpr(self):
            return self.getTypedRuleContext(PSParser.SubexprContext,0)


        def supexpr(self):
            return self.getTypedRuleContext(PSParser.SupexprContext,0)


        def FUNC_INT(self):
            return self.getToken(PSParser.FUNC_INT, 0)

        def DIFFERENTIAL(self):
            return self.getToken(PSParser.DIFFERENTIAL, 0)

        def frac(self):
            return self.getTypedRuleContext(PSParser.FracContext,0)


        def additive(self):
            return self.getTypedRuleContext(PSParser.AdditiveContext,0)


        def FUNC_SQRT(self):
            return self.getToken(PSParser.FUNC_SQRT, 0)

        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def mp(self):
            return self.getTypedRuleContext(PSParser.MpContext,0)


        def FUNC_SUM(self):
            return self.getToken(PSParser.FUNC_SUM, 0)

        def FUNC_PROD(self):
            return self.getToken(PSParser.FUNC_PROD, 0)

        def subeq(self):
            return self.getTypedRuleContext(PSParser.SubeqContext,0)


        def FUNC_LIM(self):
            return self.getToken(PSParser.FUNC_LIM, 0)

        def limit_sub(self):
            return self.getTypedRuleContext(PSParser.Limit_subContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_func

        def enterRule(self, listener):
            if hasattr(listener, "enterFunc"):
                listener.enterFunc(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunc"):
                listener.exitFunc(self)




    def func(self):

        localctx = PSParser.FuncContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_func)
        self._la = 0 # Token type
        try:
            self.state = 259
            token = self._input.LA(1)
            if token in [PSParser.FUNC_LOG, PSParser.FUNC_LN, PSParser.FUNC_SIN, PSParser.FUNC_COS, PSParser.FUNC_TAN, PSParser.FUNC_CSC, PSParser.FUNC_SEC, PSParser.FUNC_COT, PSParser.FUNC_ARCSIN, PSParser.FUNC_ARCCOS, PSParser.FUNC_ARCTAN, PSParser.FUNC_ARCCSC, PSParser.FUNC_ARCSEC, PSParser.FUNC_ARCCOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 205
                self.func_normal()
                self.state = 218
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,24,self._ctx)
                if la_ == 1:
                    self.state = 207
                    _la = self._input.LA(1)
                    if _la==PSParser.UNDERSCORE:
                        self.state = 206
                        self.subexpr()


                    self.state = 210
                    _la = self._input.LA(1)
                    if _la==PSParser.CARET:
                        self.state = 209
                        self.supexpr()


                    pass

                elif la_ == 2:
                    self.state = 213
                    _la = self._input.LA(1)
                    if _la==PSParser.CARET:
                        self.state = 212
                        self.supexpr()


                    self.state = 216
                    _la = self._input.LA(1)
                    if _la==PSParser.UNDERSCORE:
                        self.state = 215
                        self.subexpr()


                    pass


                self.state = 220
                self.func_arg()

            elif token in [PSParser.FUNC_INT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 222
                self.match(PSParser.FUNC_INT)
                self.state = 229
                token = self._input.LA(1)
                if token in [PSParser.UNDERSCORE]:
                    self.state = 223
                    self.subexpr()
                    self.state = 224
                    self.supexpr()
                    pass
                elif token in [PSParser.CARET]:
                    self.state = 226
                    self.supexpr()
                    self.state = 227
                    self.subexpr()
                    pass
                elif token in [PSParser.ADD, PSParser.SUB, PSParser.L_PAREN, PSParser.L_BRACKET, PSParser.BAR, PSParser.FUNC_LIM, PSParser.FUNC_INT, PSParser.FUNC_SUM, PSParser.FUNC_PROD, PSParser.FUNC_LOG, PSParser.FUNC_LN, PSParser.FUNC_SIN, PSParser.FUNC_COS, PSParser.FUNC_TAN, PSParser.FUNC_CSC, PSParser.FUNC_SEC, PSParser.FUNC_COT, PSParser.FUNC_ARCSIN, PSParser.FUNC_ARCCOS, PSParser.FUNC_ARCTAN, PSParser.FUNC_ARCCSC, PSParser.FUNC_ARCSEC, PSParser.FUNC_ARCCOT, PSParser.FUNC_SQRT, PSParser.CMD_FRAC, PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                    pass
                else:
                    raise NoViableAltException(self)
                self.state = 237
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,27,self._ctx)
                if la_ == 1:
                    self.state = 232
                    self._errHandler.sync(self);
                    la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
                    if la_ == 1:
                        self.state = 231
                        self.additive(0)


                    self.state = 234
                    self.match(PSParser.DIFFERENTIAL)
                    pass

                elif la_ == 2:
                    self.state = 235
                    self.frac()
                    pass

                elif la_ == 3:
                    self.state = 236
                    self.additive(0)
                    pass



            elif token in [PSParser.FUNC_SQRT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 239
                self.match(PSParser.FUNC_SQRT)
                self.state = 240
                self.match(PSParser.L_BRACE)
                self.state = 241
                self.expr()
                self.state = 242
                self.match(PSParser.R_BRACE)

            elif token in [PSParser.FUNC_SUM, PSParser.FUNC_PROD]:
                self.enterOuterAlt(localctx, 4)
                self.state = 244
                _la = self._input.LA(1)
                if not(_la==PSParser.FUNC_SUM or _la==PSParser.FUNC_PROD):
                    self._errHandler.recoverInline(self)
                else:
                    self.consume()
                self.state = 251
                token = self._input.LA(1)
                if token in [PSParser.UNDERSCORE]:
                    self.state = 245
                    self.subeq()
                    self.state = 246
                    self.supexpr()

                elif token in [PSParser.CARET]:
                    self.state = 248
                    self.supexpr()
                    self.state = 249
                    self.subeq()

                else:
                    raise NoViableAltException(self)

                self.state = 253
                self.mp(0)

            elif token in [PSParser.FUNC_LIM]:
                self.enterOuterAlt(localctx, 5)
                self.state = 255
                self.match(PSParser.FUNC_LIM)
                self.state = 256
                self.limit_sub()
                self.state = 257
                self.mp(0)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Limit_subContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Limit_subContext, self).__init__(parent, invokingState)
            self.parser = parser

        def UNDERSCORE(self):
            return self.getToken(PSParser.UNDERSCORE, 0)

        def L_BRACE(self, i=None):
            if i is None:
                return self.getTokens(PSParser.L_BRACE)
            else:
                return self.getToken(PSParser.L_BRACE, i)

        def LIM_APPROACH_SYM(self):
            return self.getToken(PSParser.LIM_APPROACH_SYM, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_BRACE(self, i=None):
            if i is None:
                return self.getTokens(PSParser.R_BRACE)
            else:
                return self.getToken(PSParser.R_BRACE, i)

        def LETTER(self):
            return self.getToken(PSParser.LETTER, 0)

        def SYMBOL(self):
            return self.getToken(PSParser.SYMBOL, 0)

        def CARET(self):
            return self.getToken(PSParser.CARET, 0)

        def ADD(self):
            return self.getToken(PSParser.ADD, 0)

        def SUB(self):
            return self.getToken(PSParser.SUB, 0)

        def getRuleIndex(self):
            return PSParser.RULE_limit_sub

        def enterRule(self, listener):
            if hasattr(listener, "enterLimit_sub"):
                listener.enterLimit_sub(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitLimit_sub"):
                listener.exitLimit_sub(self)




    def limit_sub(self):

        localctx = PSParser.Limit_subContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_limit_sub)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 261
            self.match(PSParser.UNDERSCORE)
            self.state = 262
            self.match(PSParser.L_BRACE)
            self.state = 263
            _la = self._input.LA(1)
            if not(_la==PSParser.LETTER or _la==PSParser.SYMBOL):
                self._errHandler.recoverInline(self)
            else:
                self.consume()
            self.state = 264
            self.match(PSParser.LIM_APPROACH_SYM)
            self.state = 265
            self.expr()
            self.state = 270
            _la = self._input.LA(1)
            if _la==PSParser.CARET:
                self.state = 266
                self.match(PSParser.CARET)
                self.state = 267
                self.match(PSParser.L_BRACE)
                self.state = 268
                _la = self._input.LA(1)
                if not(_la==PSParser.ADD or _la==PSParser.SUB):
                    self._errHandler.recoverInline(self)
                else:
                    self.consume()
                self.state = 269
                self.match(PSParser.R_BRACE)


            self.state = 272
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Func_argContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.Func_argContext, self).__init__(parent, invokingState)
            self.parser = parser

        def atom(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(PSParser.AtomContext)
            else:
                return self.getTypedRuleContext(PSParser.AtomContext,i)


        def comp(self):
            return self.getTypedRuleContext(PSParser.CompContext,0)


        def getRuleIndex(self):
            return PSParser.RULE_func_arg

        def enterRule(self, listener):
            if hasattr(listener, "enterFunc_arg"):
                listener.enterFunc_arg(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunc_arg"):
                listener.exitFunc_arg(self)




    def func_arg(self):

        localctx = PSParser.Func_argContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_func_arg)
        try:
            self.state = 280
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 275 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 274
                        self.atom()

                    else:
                        raise NoViableAltException(self)
                    self.state = 277 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 279
                self.comp()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SubexprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.SubexprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def UNDERSCORE(self):
            return self.getToken(PSParser.UNDERSCORE, 0)

        def atom(self):
            return self.getTypedRuleContext(PSParser.AtomContext,0)


        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def getRuleIndex(self):
            return PSParser.RULE_subexpr

        def enterRule(self, listener):
            if hasattr(listener, "enterSubexpr"):
                listener.enterSubexpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSubexpr"):
                listener.exitSubexpr(self)




    def subexpr(self):

        localctx = PSParser.SubexprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_subexpr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 282
            self.match(PSParser.UNDERSCORE)
            self.state = 288
            token = self._input.LA(1)
            if token in [PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                self.state = 283
                self.atom()

            elif token in [PSParser.L_BRACE]:
                self.state = 284
                self.match(PSParser.L_BRACE)
                self.state = 285
                self.expr()
                self.state = 286
                self.match(PSParser.R_BRACE)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SupexprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.SupexprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def CARET(self):
            return self.getToken(PSParser.CARET, 0)

        def atom(self):
            return self.getTypedRuleContext(PSParser.AtomContext,0)


        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def expr(self):
            return self.getTypedRuleContext(PSParser.ExprContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def getRuleIndex(self):
            return PSParser.RULE_supexpr

        def enterRule(self, listener):
            if hasattr(listener, "enterSupexpr"):
                listener.enterSupexpr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSupexpr"):
                listener.exitSupexpr(self)




    def supexpr(self):

        localctx = PSParser.SupexprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_supexpr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 290
            self.match(PSParser.CARET)
            self.state = 296
            token = self._input.LA(1)
            if token in [PSParser.DIFFERENTIAL, PSParser.LETTER, PSParser.NUMBER, PSParser.SYMBOL]:
                self.state = 291
                self.atom()

            elif token in [PSParser.L_BRACE]:
                self.state = 292
                self.match(PSParser.L_BRACE)
                self.state = 293
                self.expr()
                self.state = 294
                self.match(PSParser.R_BRACE)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SubeqContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.SubeqContext, self).__init__(parent, invokingState)
            self.parser = parser

        def UNDERSCORE(self):
            return self.getToken(PSParser.UNDERSCORE, 0)

        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def equality(self):
            return self.getTypedRuleContext(PSParser.EqualityContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def getRuleIndex(self):
            return PSParser.RULE_subeq

        def enterRule(self, listener):
            if hasattr(listener, "enterSubeq"):
                listener.enterSubeq(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSubeq"):
                listener.exitSubeq(self)




    def subeq(self):

        localctx = PSParser.SubeqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_subeq)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 298
            self.match(PSParser.UNDERSCORE)
            self.state = 299
            self.match(PSParser.L_BRACE)
            self.state = 300
            self.equality()
            self.state = 301
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SupeqContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(PSParser.SupeqContext, self).__init__(parent, invokingState)
            self.parser = parser

        def UNDERSCORE(self):
            return self.getToken(PSParser.UNDERSCORE, 0)

        def L_BRACE(self):
            return self.getToken(PSParser.L_BRACE, 0)

        def equality(self):
            return self.getTypedRuleContext(PSParser.EqualityContext,0)


        def R_BRACE(self):
            return self.getToken(PSParser.R_BRACE, 0)

        def getRuleIndex(self):
            return PSParser.RULE_supeq

        def enterRule(self, listener):
            if hasattr(listener, "enterSupeq"):
                listener.enterSupeq(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSupeq"):
                listener.exitSupeq(self)




    def supeq(self):

        localctx = PSParser.SupeqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_supeq)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 303
            self.match(PSParser.UNDERSCORE)
            self.state = 304
            self.match(PSParser.L_BRACE)
            self.state = 305
            self.equality()
            self.state = 306
            self.match(PSParser.R_BRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx, ruleIndex, predIndex):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.relation_sempred
        self._predicates[4] = self.additive_sempred
        self._predicates[5] = self.mp_sempred
        self._predicates[12] = self.exp_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def relation_sempred(self, localctx, predIndex):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def additive_sempred(self, localctx, predIndex):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def mp_sempred(self, localctx, predIndex):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

    def exp_sempred(self, localctx, predIndex):
            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         




