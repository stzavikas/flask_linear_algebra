# Generated from PS.g4 by ANTLR 4.5.3
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2")
        buf.write(u"\62\u0178\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6")
        buf.write(u"\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4")
        buf.write(u"\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t")
        buf.write(u"\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27")
        buf.write(u"\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4")
        buf.write(u"\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t")
        buf.write(u"#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4")
        buf.write(u",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62")
        buf.write(u"\3\2\6\2g\n\2\r\2\16\2h\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3")
        buf.write(u"\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13")
        buf.write(u"\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3")
        buf.write(u"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write(u"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3")
        buf.write(u"\17\3\17\5\17\u00a1\n\17\3\20\3\20\3\20\3\20\3\20\3\21")
        buf.write(u"\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3")
        buf.write(u"\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25")
        buf.write(u"\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3")
        buf.write(u"\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31")
        buf.write(u"\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3")
        buf.write(u"\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write(u"\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3")
        buf.write(u"\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37")
        buf.write(u"\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3!")
        buf.write(u"\3!\3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#")
        buf.write(u"\3#\3#\3#\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3\'")
        buf.write(u"\3\'\3(\3(\3(\3(\6(\u0130\n(\r(\16(\u0131\5(\u0134\n")
        buf.write(u"(\3)\3)\3*\3*\3+\6+\u013b\n+\r+\16+\u013c\3+\3+\3+\3")
        buf.write(u"+\3+\7+\u0144\n+\f+\16+\u0147\13+\3+\7+\u014a\n+\f+\16")
        buf.write(u"+\u014d\13+\3+\3+\3+\3+\3+\7+\u0154\n+\f+\16+\u0157\13")
        buf.write(u"+\3+\3+\6+\u015b\n+\r+\16+\u015c\5+\u015f\n+\3,\3,\3")
        buf.write(u"-\3-\3.\3.\3.\3.\3.\3/\3/\3\60\3\60\3\60\3\60\3\60\3")
        buf.write(u"\61\3\61\3\62\3\62\6\62\u0175\n\62\r\62\16\62\u0176\2")
        buf.write(u"\2\63\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27")
        buf.write(u"\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-")
        buf.write(u"\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%")
        buf.write(u"I&K\'M(O)Q*S\2U+W,Y-[.]/_\60a\61c\62\3\2\5\5\2\13\f\17")
        buf.write(u"\17\"\"\4\2C\\c|\3\2\62;\u0182\2\3\3\2\2\2\2\5\3\2\2")
        buf.write(u"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2")
        buf.write(u"\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2")
        buf.write(u"\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2")
        buf.write(u"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2")
        buf.write(u"\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61")
        buf.write(u"\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3")
        buf.write(u"\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2")
        buf.write(u"C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2")
        buf.write(u"\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2U\3\2\2\2\2W\3\2\2")
        buf.write(u"\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2")
        buf.write(u"\2\2\2c\3\2\2\2\3f\3\2\2\2\5l\3\2\2\2\7n\3\2\2\2\tp\3")
        buf.write(u"\2\2\2\13r\3\2\2\2\rt\3\2\2\2\17v\3\2\2\2\21x\3\2\2\2")
        buf.write(u"\23z\3\2\2\2\25|\3\2\2\2\27~\3\2\2\2\31\u0080\3\2\2\2")
        buf.write(u"\33\u0082\3\2\2\2\35\u00a0\3\2\2\2\37\u00a2\3\2\2\2!")
        buf.write(u"\u00a7\3\2\2\2#\u00ac\3\2\2\2%\u00b2\3\2\2\2\'\u00b7")
        buf.write(u"\3\2\2\2)\u00bb\3\2\2\2+\u00c0\3\2\2\2-\u00c5\3\2\2\2")
        buf.write(u"/\u00ca\3\2\2\2\61\u00cf\3\2\2\2\63\u00d4\3\2\2\2\65")
        buf.write(u"\u00d9\3\2\2\2\67\u00e1\3\2\2\29\u00e9\3\2\2\2;\u00f1")
        buf.write(u"\3\2\2\2=\u00f9\3\2\2\2?\u0101\3\2\2\2A\u0109\3\2\2\2")
        buf.write(u"C\u010f\3\2\2\2E\u0116\3\2\2\2G\u011c\3\2\2\2I\u0121")
        buf.write(u"\3\2\2\2K\u0127\3\2\2\2M\u0129\3\2\2\2O\u012b\3\2\2\2")
        buf.write(u"Q\u0135\3\2\2\2S\u0137\3\2\2\2U\u015e\3\2\2\2W\u0160")
        buf.write(u"\3\2\2\2Y\u0162\3\2\2\2[\u0164\3\2\2\2]\u0169\3\2\2\2")
        buf.write(u"_\u016b\3\2\2\2a\u0170\3\2\2\2c\u0172\3\2\2\2eg\t\2\2")
        buf.write(u"\2fe\3\2\2\2gh\3\2\2\2hf\3\2\2\2hi\3\2\2\2ij\3\2\2\2")
        buf.write(u"jk\b\2\2\2k\4\3\2\2\2lm\7-\2\2m\6\3\2\2\2no\7/\2\2o\b")
        buf.write(u"\3\2\2\2pq\7,\2\2q\n\3\2\2\2rs\7\61\2\2s\f\3\2\2\2tu")
        buf.write(u"\7*\2\2u\16\3\2\2\2vw\7+\2\2w\20\3\2\2\2xy\7}\2\2y\22")
        buf.write(u"\3\2\2\2z{\7\177\2\2{\24\3\2\2\2|}\7]\2\2}\26\3\2\2\2")
        buf.write(u"~\177\7_\2\2\177\30\3\2\2\2\u0080\u0081\7~\2\2\u0081")
        buf.write(u"\32\3\2\2\2\u0082\u0083\7^\2\2\u0083\u0084\7n\2\2\u0084")
        buf.write(u"\u0085\7k\2\2\u0085\u0086\7o\2\2\u0086\34\3\2\2\2\u0087")
        buf.write(u"\u0088\7^\2\2\u0088\u0089\7v\2\2\u0089\u00a1\7q\2\2\u008a")
        buf.write(u"\u008b\7^\2\2\u008b\u008c\7t\2\2\u008c\u008d\7k\2\2\u008d")
        buf.write(u"\u008e\7i\2\2\u008e\u008f\7j\2\2\u008f\u0090\7v\2\2\u0090")
        buf.write(u"\u0091\7c\2\2\u0091\u0092\7t\2\2\u0092\u0093\7t\2\2\u0093")
        buf.write(u"\u0094\7q\2\2\u0094\u00a1\7y\2\2\u0095\u0096\7^\2\2\u0096")
        buf.write(u"\u0097\7T\2\2\u0097\u0098\7k\2\2\u0098\u0099\7i\2\2\u0099")
        buf.write(u"\u009a\7j\2\2\u009a\u009b\7v\2\2\u009b\u009c\7c\2\2\u009c")
        buf.write(u"\u009d\7t\2\2\u009d\u009e\7t\2\2\u009e\u009f\7q\2\2\u009f")
        buf.write(u"\u00a1\7y\2\2\u00a0\u0087\3\2\2\2\u00a0\u008a\3\2\2\2")
        buf.write(u"\u00a0\u0095\3\2\2\2\u00a1\36\3\2\2\2\u00a2\u00a3\7^")
        buf.write(u"\2\2\u00a3\u00a4\7k\2\2\u00a4\u00a5\7p\2\2\u00a5\u00a6")
        buf.write(u"\7v\2\2\u00a6 \3\2\2\2\u00a7\u00a8\7^\2\2\u00a8\u00a9")
        buf.write(u"\7u\2\2\u00a9\u00aa\7w\2\2\u00aa\u00ab\7o\2\2\u00ab\"")
        buf.write(u"\3\2\2\2\u00ac\u00ad\7^\2\2\u00ad\u00ae\7r\2\2\u00ae")
        buf.write(u"\u00af\7t\2\2\u00af\u00b0\7q\2\2\u00b0\u00b1\7f\2\2\u00b1")
        buf.write(u"$\3\2\2\2\u00b2\u00b3\7^\2\2\u00b3\u00b4\7n\2\2\u00b4")
        buf.write(u"\u00b5\7q\2\2\u00b5\u00b6\7i\2\2\u00b6&\3\2\2\2\u00b7")
        buf.write(u"\u00b8\7^\2\2\u00b8\u00b9\7n\2\2\u00b9\u00ba\7p\2\2\u00ba")
        buf.write(u"(\3\2\2\2\u00bb\u00bc\7^\2\2\u00bc\u00bd\7u\2\2\u00bd")
        buf.write(u"\u00be\7k\2\2\u00be\u00bf\7p\2\2\u00bf*\3\2\2\2\u00c0")
        buf.write(u"\u00c1\7^\2\2\u00c1\u00c2\7e\2\2\u00c2\u00c3\7q\2\2\u00c3")
        buf.write(u"\u00c4\7u\2\2\u00c4,\3\2\2\2\u00c5\u00c6\7^\2\2\u00c6")
        buf.write(u"\u00c7\7v\2\2\u00c7\u00c8\7c\2\2\u00c8\u00c9\7p\2\2\u00c9")
        buf.write(u".\3\2\2\2\u00ca\u00cb\7^\2\2\u00cb\u00cc\7e\2\2\u00cc")
        buf.write(u"\u00cd\7u\2\2\u00cd\u00ce\7e\2\2\u00ce\60\3\2\2\2\u00cf")
        buf.write(u"\u00d0\7^\2\2\u00d0\u00d1\7u\2\2\u00d1\u00d2\7g\2\2\u00d2")
        buf.write(u"\u00d3\7e\2\2\u00d3\62\3\2\2\2\u00d4\u00d5\7^\2\2\u00d5")
        buf.write(u"\u00d6\7e\2\2\u00d6\u00d7\7q\2\2\u00d7\u00d8\7v\2\2\u00d8")
        buf.write(u"\64\3\2\2\2\u00d9\u00da\7^\2\2\u00da\u00db\7c\2\2\u00db")
        buf.write(u"\u00dc\7t\2\2\u00dc\u00dd\7e\2\2\u00dd\u00de\7u\2\2\u00de")
        buf.write(u"\u00df\7k\2\2\u00df\u00e0\7p\2\2\u00e0\66\3\2\2\2\u00e1")
        buf.write(u"\u00e2\7^\2\2\u00e2\u00e3\7c\2\2\u00e3\u00e4\7t\2\2\u00e4")
        buf.write(u"\u00e5\7e\2\2\u00e5\u00e6\7e\2\2\u00e6\u00e7\7q\2\2\u00e7")
        buf.write(u"\u00e8\7u\2\2\u00e88\3\2\2\2\u00e9\u00ea\7^\2\2\u00ea")
        buf.write(u"\u00eb\7c\2\2\u00eb\u00ec\7t\2\2\u00ec\u00ed\7e\2\2\u00ed")
        buf.write(u"\u00ee\7v\2\2\u00ee\u00ef\7c\2\2\u00ef\u00f0\7p\2\2\u00f0")
        buf.write(u":\3\2\2\2\u00f1\u00f2\7^\2\2\u00f2\u00f3\7c\2\2\u00f3")
        buf.write(u"\u00f4\7t\2\2\u00f4\u00f5\7e\2\2\u00f5\u00f6\7e\2\2\u00f6")
        buf.write(u"\u00f7\7u\2\2\u00f7\u00f8\7e\2\2\u00f8<\3\2\2\2\u00f9")
        buf.write(u"\u00fa\7^\2\2\u00fa\u00fb\7c\2\2\u00fb\u00fc\7t\2\2\u00fc")
        buf.write(u"\u00fd\7e\2\2\u00fd\u00fe\7u\2\2\u00fe\u00ff\7g\2\2\u00ff")
        buf.write(u"\u0100\7e\2\2\u0100>\3\2\2\2\u0101\u0102\7^\2\2\u0102")
        buf.write(u"\u0103\7c\2\2\u0103\u0104\7t\2\2\u0104\u0105\7e\2\2\u0105")
        buf.write(u"\u0106\7e\2\2\u0106\u0107\7q\2\2\u0107\u0108\7v\2\2\u0108")
        buf.write(u"@\3\2\2\2\u0109\u010a\7^\2\2\u010a\u010b\7u\2\2\u010b")
        buf.write(u"\u010c\7s\2\2\u010c\u010d\7t\2\2\u010d\u010e\7v\2\2\u010e")
        buf.write(u"B\3\2\2\2\u010f\u0110\7^\2\2\u0110\u0111\7v\2\2\u0111")
        buf.write(u"\u0112\7k\2\2\u0112\u0113\7o\2\2\u0113\u0114\7g\2\2\u0114")
        buf.write(u"\u0115\7u\2\2\u0115D\3\2\2\2\u0116\u0117\7^\2\2\u0117")
        buf.write(u"\u0118\7e\2\2\u0118\u0119\7f\2\2\u0119\u011a\7q\2\2\u011a")
        buf.write(u"\u011b\7v\2\2\u011bF\3\2\2\2\u011c\u011d\7^\2\2\u011d")
        buf.write(u"\u011e\7f\2\2\u011e\u011f\7k\2\2\u011f\u0120\7x\2\2\u0120")
        buf.write(u"H\3\2\2\2\u0121\u0122\7^\2\2\u0122\u0123\7h\2\2\u0123")
        buf.write(u"\u0124\7t\2\2\u0124\u0125\7c\2\2\u0125\u0126\7e\2\2\u0126")
        buf.write(u"J\3\2\2\2\u0127\u0128\7a\2\2\u0128L\3\2\2\2\u0129\u012a")
        buf.write(u"\7`\2\2\u012aN\3\2\2\2\u012b\u0133\7f\2\2\u012c\u0134")
        buf.write(u"\t\3\2\2\u012d\u012f\7^\2\2\u012e\u0130\t\3\2\2\u012f")
        buf.write(u"\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u012f\3\2\2")
        buf.write(u"\2\u0131\u0132\3\2\2\2\u0132\u0134\3\2\2\2\u0133\u012c")
        buf.write(u"\3\2\2\2\u0133\u012d\3\2\2\2\u0134P\3\2\2\2\u0135\u0136")
        buf.write(u"\t\3\2\2\u0136R\3\2\2\2\u0137\u0138\t\4\2\2\u0138T\3")
        buf.write(u"\2\2\2\u0139\u013b\5S*\2\u013a\u0139\3\2\2\2\u013b\u013c")
        buf.write(u"\3\2\2\2\u013c\u013a\3\2\2\2\u013c\u013d\3\2\2\2\u013d")
        buf.write(u"\u0145\3\2\2\2\u013e\u013f\7.\2\2\u013f\u0140\5S*\2\u0140")
        buf.write(u"\u0141\5S*\2\u0141\u0142\5S*\2\u0142\u0144\3\2\2\2\u0143")
        buf.write(u"\u013e\3\2\2\2\u0144\u0147\3\2\2\2\u0145\u0143\3\2\2")
        buf.write(u"\2\u0145\u0146\3\2\2\2\u0146\u015f\3\2\2\2\u0147\u0145")
        buf.write(u"\3\2\2\2\u0148\u014a\5S*\2\u0149\u0148\3\2\2\2\u014a")
        buf.write(u"\u014d\3\2\2\2\u014b\u0149\3\2\2\2\u014b\u014c\3\2\2")
        buf.write(u"\2\u014c\u0155\3\2\2\2\u014d\u014b\3\2\2\2\u014e\u014f")
        buf.write(u"\7.\2\2\u014f\u0150\5S*\2\u0150\u0151\5S*\2\u0151\u0152")
        buf.write(u"\5S*\2\u0152\u0154\3\2\2\2\u0153\u014e\3\2\2\2\u0154")
        buf.write(u"\u0157\3\2\2\2\u0155\u0153\3\2\2\2\u0155\u0156\3\2\2")
        buf.write(u"\2\u0156\u0158\3\2\2\2\u0157\u0155\3\2\2\2\u0158\u015a")
        buf.write(u"\7\60\2\2\u0159\u015b\5S*\2\u015a\u0159\3\2\2\2\u015b")
        buf.write(u"\u015c\3\2\2\2\u015c\u015a\3\2\2\2\u015c\u015d\3\2\2")
        buf.write(u"\2\u015d\u015f\3\2\2\2\u015e\u013a\3\2\2\2\u015e\u014b")
        buf.write(u"\3\2\2\2\u015fV\3\2\2\2\u0160\u0161\7?\2\2\u0161X\3\2")
        buf.write(u"\2\2\u0162\u0163\7>\2\2\u0163Z\3\2\2\2\u0164\u0165\7")
        buf.write(u"^\2\2\u0165\u0166\7n\2\2\u0166\u0167\7g\2\2\u0167\u0168")
        buf.write(u"\7s\2\2\u0168\\\3\2\2\2\u0169\u016a\7@\2\2\u016a^\3\2")
        buf.write(u"\2\2\u016b\u016c\7^\2\2\u016c\u016d\7i\2\2\u016d\u016e")
        buf.write(u"\7g\2\2\u016e\u016f\7s\2\2\u016f`\3\2\2\2\u0170\u0171")
        buf.write(u"\7#\2\2\u0171b\3\2\2\2\u0172\u0174\7^\2\2\u0173\u0175")
        buf.write(u"\t\3\2\2\u0174\u0173\3\2\2\2\u0175\u0176\3\2\2\2\u0176")
        buf.write(u"\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177d\3\2\2\2\16")
        buf.write(u"\2h\u00a0\u0131\u0133\u013c\u0145\u014b\u0155\u015c\u015e")
        buf.write(u"\u0176\3\b\2\2")
        return buf.getvalue()


class PSLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    WS = 1
    ADD = 2
    SUB = 3
    MUL = 4
    DIV = 5
    L_PAREN = 6
    R_PAREN = 7
    L_BRACE = 8
    R_BRACE = 9
    L_BRACKET = 10
    R_BRACKET = 11
    BAR = 12
    FUNC_LIM = 13
    LIM_APPROACH_SYM = 14
    FUNC_INT = 15
    FUNC_SUM = 16
    FUNC_PROD = 17
    FUNC_LOG = 18
    FUNC_LN = 19
    FUNC_SIN = 20
    FUNC_COS = 21
    FUNC_TAN = 22
    FUNC_CSC = 23
    FUNC_SEC = 24
    FUNC_COT = 25
    FUNC_ARCSIN = 26
    FUNC_ARCCOS = 27
    FUNC_ARCTAN = 28
    FUNC_ARCCSC = 29
    FUNC_ARCSEC = 30
    FUNC_ARCCOT = 31
    FUNC_SQRT = 32
    CMD_TIMES = 33
    CMD_CDOT = 34
    CMD_DIV = 35
    CMD_FRAC = 36
    UNDERSCORE = 37
    CARET = 38
    DIFFERENTIAL = 39
    LETTER = 40
    NUMBER = 41
    EQUAL = 42
    LT = 43
    LTE = 44
    GT = 45
    GTE = 46
    BANG = 47
    SYMBOL = 48

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'+'", u"'-'", u"'*'", u"'/'", u"'('", u"')'", u"'{'", u"'}'", 
            u"'['", u"']'", u"'|'", u"'\\lim'", u"'\\int'", u"'\\sum'", 
            u"'\\prod'", u"'\\log'", u"'\\ln'", u"'\\sin'", u"'\\cos'", 
            u"'\\tan'", u"'\\csc'", u"'\\sec'", u"'\\cot'", u"'\\arcsin'", 
            u"'\\arccos'", u"'\\arctan'", u"'\\arccsc'", u"'\\arcsec'", 
            u"'\\arccot'", u"'\\sqrt'", u"'\\times'", u"'\\cdot'", u"'\\div'", 
            u"'\\frac'", u"'_'", u"'^'", u"'='", u"'<'", u"'\\leq'", u"'>'", 
            u"'\\geq'", u"'!'" ]

    symbolicNames = [ u"<INVALID>",
            u"WS", u"ADD", u"SUB", u"MUL", u"DIV", u"L_PAREN", u"R_PAREN", 
            u"L_BRACE", u"R_BRACE", u"L_BRACKET", u"R_BRACKET", u"BAR", 
            u"FUNC_LIM", u"LIM_APPROACH_SYM", u"FUNC_INT", u"FUNC_SUM", 
            u"FUNC_PROD", u"FUNC_LOG", u"FUNC_LN", u"FUNC_SIN", u"FUNC_COS", 
            u"FUNC_TAN", u"FUNC_CSC", u"FUNC_SEC", u"FUNC_COT", u"FUNC_ARCSIN", 
            u"FUNC_ARCCOS", u"FUNC_ARCTAN", u"FUNC_ARCCSC", u"FUNC_ARCSEC", 
            u"FUNC_ARCCOT", u"FUNC_SQRT", u"CMD_TIMES", u"CMD_CDOT", u"CMD_DIV", 
            u"CMD_FRAC", u"UNDERSCORE", u"CARET", u"DIFFERENTIAL", u"LETTER", 
            u"NUMBER", u"EQUAL", u"LT", u"LTE", u"GT", u"GTE", u"BANG", 
            u"SYMBOL" ]

    ruleNames = [ u"WS", u"ADD", u"SUB", u"MUL", u"DIV", u"L_PAREN", u"R_PAREN", 
                  u"L_BRACE", u"R_BRACE", u"L_BRACKET", u"R_BRACKET", u"BAR", 
                  u"FUNC_LIM", u"LIM_APPROACH_SYM", u"FUNC_INT", u"FUNC_SUM", 
                  u"FUNC_PROD", u"FUNC_LOG", u"FUNC_LN", u"FUNC_SIN", u"FUNC_COS", 
                  u"FUNC_TAN", u"FUNC_CSC", u"FUNC_SEC", u"FUNC_COT", u"FUNC_ARCSIN", 
                  u"FUNC_ARCCOS", u"FUNC_ARCTAN", u"FUNC_ARCCSC", u"FUNC_ARCSEC", 
                  u"FUNC_ARCCOT", u"FUNC_SQRT", u"CMD_TIMES", u"CMD_CDOT", 
                  u"CMD_DIV", u"CMD_FRAC", u"UNDERSCORE", u"CARET", u"DIFFERENTIAL", 
                  u"LETTER", u"DIGIT", u"NUMBER", u"EQUAL", u"LT", u"LTE", 
                  u"GT", u"GTE", u"BANG", u"SYMBOL" ]

    grammarFileName = u"PS.g4"

    def __init__(self, input=None):
        super(PSLexer, self).__init__(input)
        self.checkVersion("4.5.3")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


