from ast import literal_eval
from scipy.linalg import lu, svd, cholesky, polar, qr, rq, schur, hessenberg, eigvals
from flask import Blueprint, request, jsonify
import json

lu_page = Blueprint('lu', __name__)
svd_page = Blueprint('svd', __name__)
cholesky_page = Blueprint('cholesky', __name__)
polar_page = Blueprint('polar', __name__)
qr_page = Blueprint('qr', __name__)
rq_page = Blueprint('rq', __name__)
schur_page = Blueprint('schur', __name__)
hessenberg_page = Blueprint('hessenberg', __name__)
eigenvalues_page = Blueprint('eigenvalues', __name__)


@lu_page.route('/', methods=['POST', 'GET'])
def lu_method():
    if request.method == 'GET':
        return "LU Decomposition helper."
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return lu_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in LU decomposition."
    else:
        return "Unsupported method."


@svd_page.route('/', methods=['POST', 'GET'])
def svd_method():
    if request.method == 'GET':
        return "SVD Decomposition helper."
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return svd_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in SVD decomposition."
    else:
        return "Unsupported method."


@cholesky_page.route('/', methods=['POST', 'GET'])
def cholesky_method():
    if request.method == 'GET':
        return "Cholesky Decomposition helper."
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return cholesky_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in cholesky decomposition."
    else:
        return "Unsupported method."


@polar_page.route('/', methods=['POST', 'GET'])
def polar_method():
    if request.method == 'GET':
        return "Polar Decomposition helper."
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return polar_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in polar decomposition."
    else:
        return "Unsupported method."


@qr_page.route('/', methods=['POST', 'GET'])
def qr_method():
    if request.method == 'GET':
        return "QR Decomposition helper."
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return qr_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in qr decomposition."
    else:
        return "Unsupported method."


@rq_page.route('/', methods=['POST', 'GET'])
def rq_method():
    if request.method == 'GET':
        return 'RQ Decomposition helper.'
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return rq_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in RQ decomposition."
    else:
        return "Unsupported method."


@schur_page.route('/', methods=['POST', 'GET'])
def schur_method():
    if request.method == 'GET':
        return 'Schur Decomposition helper.'
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return schur_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in Schur  decomposition."
    else:
        return "Unsupported method."


@hessenberg_page.route('/', methods=['POST', 'GET'])
def hessenberg_method():
    if request.method == 'GET':
        return 'Hessenberg Decomposition helper.'
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return hessenberg_decompose(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in Hessenberg decomposition."
    else:
        return "Unsupported method."


@eigenvalues_page.route('/', methods=['POST', 'GET'])
def eigenvalues_method():
    if request.method == 'GET':
        return 'Eigenvalues calculation helper.'
    elif request.method == 'POST':
        try:
            input_matrix = literal_eval(request.data.decode('utf-8'))['input']
            return eigenvalues(literal_eval(input_matrix))
        except Exception as e:
            print(str(e))
            return "Error in Eigenvalues calculation."
    else:
        return "Unsupported method."


def lu_decompose(input_matrix):
    P, L, U = lu(input_matrix)
    return str({ "P": str(P.tolist()), "L": str(L.tolist()), "U": str(U.tolist())})


def svd_decompose(input_matrix):
    U, s, Vh = svd(input_matrix)
    print("SVD: " + str(U) + " | " + str(s))
    return str({ "U": str(U.tolist()), "s": str(s.tolist()), "Vh": str(Vh.tolist())})


def cholesky_decompose(input_matrix):
    L = cholesky(input_matrix)
    return str({ "L": str(L.tolist())})


def polar_decompose(input_matrix):
    U, P = polar(input_matrix)
    return str({ "U": str(U.tolist()), "P": str(P.tolist())})


def qr_decompose(input_matrix):
    Q, R = qr(input_matrix)
    return str({ "Q": str(Q.tolist()), "R": str(R.tolist())})


def rq_decompose(input_matrix):
    R, Q = rq(input_matrix)
    return str({ "R": str(R.tolist()), "Q": str(Q.tolist())})


def schur_decompose(input_matrix):
    T, Z = schur(input_matrix)
    return str({ "T": str(T.tolist()), "Z": str(Z.tolist())})


def hessenberg_decompose(input_matrix):
    H, Q = hessenberg(input_matrix)
    return str({ "H": str(H.tolist()), "Q": str(Q.tolist())})


def eigenvalues(input_matrix):
    w = eigvals(input_matrix)
    w_str = json.dumps([w.tolist()]).replace('(','').replace(')','')
    print(w_str)
    return jsonify({ "w": w_str})
