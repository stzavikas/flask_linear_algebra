'use strict';

var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');

process.env.NODE_ENV = 'production';

var customOpts = {
  entries: ['./routes.js'],
  debug: false
};

var b = watchify(browserify(customOpts).transform(babelify, {presets: ["es2015", "react"], compact: true }));

gulp.task('js', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal

function bundle() {
  return b.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you don't need to buffer file contents
    .pipe(buffer())
    .pipe(gulp.dest('./'));
}
