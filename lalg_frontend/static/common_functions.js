'use strict';

var React = require("react");
var $ = require("jQuery");


module.exports.createMatrixFromDimensions = function(rows, columns, initial_matrix) {
    var copy = new Array(rows);
    for (var i = 0; i < rows; ++i) {
        copy[i] = new Array(columns);
        for (var j = 0; j < columns; ++j) {
            if ((initial_matrix.length > i) && (initial_matrix[i].length > j)) {
              copy[i][j] = initial_matrix[i][j];
            }
            else {
              copy[i][j] = 0;
            }
        }
    }
    return copy;
}

module.exports.getCellDataFromMatrix = function (react_matrix_module) {
    var rows = react_matrix_module.getHeight();
    var columns = react_matrix_module.getWidth();
    var output = new Array(rows);
    for(var i = 0; i < rows; ++i) {
        output[i] = new Array(columns);
        for (var j = 0; j < columns; ++j) {
            output[i][j] = parseFloat(react_matrix_module.getCellValue(i, j));
        }
    }
    return output;
}

module.exports.getArrayFromCellReactMatrixComponent = function(react_matrix_module) {
    var matrix = this.getCellDataFromMatrix(react_matrix_module);
    return this.convertArrayToMatrix(matrix);
}

module.exports.sanitizeResultMatrix = function (input_matrix) {
    var row = JSON.parse(input_matrix);
    var copy = new Array(row.length);
    for (var i=0; i<row.length; ++i) {
        var column = row[i];
        copy[i] = new Array(column.length);
        for (var j=0; j < column.length; ++j) {
            copy[i][j] = parseFloat(column[j]);
        }
    }
    return copy;
}

module.exports.convertArrayToMatrix = function(arr) {
    if (arr.length == 0) { return ; }
    var matrix = new Array(arr.length / 3);
    var i = 0;
    for (i=0; i<arr.length / 3; i++) {
        var j = 0;
        matrix[i] = new Array(3);
        for (j=0; j<3; j++) {
            matrix[i][j] = arr[i*3 + j];
        }
    }
    return matrix;
}

module.exports.convertConvolutionMatrixToArray = function(matr) {
    var arr = new Array(9);
    var i = 0;
    var j = 0;
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) {
            arr[3*i + j] = matr[i][j];
        }
    }
    return arr;
}