'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var $ = require("jQuery");
var Link = require("react-router").Link;
var IndexLink = require("react-router").IndexLink;


module.exports = React.createClass({
    handleLogout() {
        $.ajax({
            url: '/logout',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function(msg) {
                window.location.href= "/login";
            }.bind(this)
        });
    },
    render() {
        return (<div id="index_id">
                    <div id="top_level_header">
                        <h1>My Shader Playground</h1>
                    </div>
                    <ul className="header">
                        <li><Link to="/homePage" activeClassName="active">Home</Link></li>
                        <li><Link to="/materialShaderPage" activeClassName="active">GLSL Material Shaders</Link></li>
                        <li><Link to="/convolutionPage" activeClassName="active">GLSL Convolution Kernels</Link></li>
                        <li><Link to="/fractalsPage" activeClassName="active">GLSL Fractals</Link></li>
                        <li><Link to="/matrixPage" activeClassName="active">Matrix Decompositions</Link></li>
                        <li><Link to="/equationPage" activeClassName="active">Linear Equations</Link></li>
                    </ul>
                    <div className="content">
                        {this.props.children}
                    </div>
                </div>);
    }
});