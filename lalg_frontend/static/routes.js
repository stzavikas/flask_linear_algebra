'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var headerPage = require('./header.js')
var matrixPage = require('./views/matrix_page.js');
var equationPage = require('./views/equation_solver.js');
var fractalsPage = require('./views/fractals_page.js');
var convolutionPage = require('./views/convolution_kernels.js');
var materialShaderPage = require('./views/material_shaders.js');

var indexPage = require('./views/index.js')

var Router = require("react-router").Router;
var Route = require("react-router").Route;
var browserHistory = require("react-router").browserHistory;
var IndexRoute = require("react-router").IndexRoute;

ReactDOM.render((<div id="my_page">
        <Router history={browserHistory}>
            <Route path="/" component={headerPage}>
                <Route name="homePage" path="/homePage" component={indexPage} />
                <Route name="matrixPage" path="/matrixPage" component={matrixPage} />
                <Route name="equationPage" path="/equationPage" component={equationPage} />
                <Route name="fractalsPage" path="/fractalsPage" component={fractalsPage} />
                <Route name="convolutionPage" path="/convolutionPage" component={convolutionPage} />
                <Route name="materialShaderPage" path="/materialShaderPage" component={materialShaderPage} />
            </Route>
        </Router></div>), document.getElementById("main_page_div"))