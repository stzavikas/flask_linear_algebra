'use strict';

var React = require("react");
var $ = require("jQuery");
var ReactDOM = require("react-dom");
var MatrixModule = require("../react_matrix.js");
var DropdownList = require("react-widgets/lib/DropdownList.js");
var NumberPicker = require("react-widgets/lib/NumberPicker.js");
var numberLocalizer = require("react-widgets/lib/localizers/simple-number.js");
var commonFuncs = require("../common_functions.js");

numberLocalizer();

var inputsStyle = {
    overflow: "hidden"
};

var leftPaneStyle = {
    float: "left",
    width: "200px"
};

var rightPaneStyle = {
    float: "left",
    padding: "0px 50px",
    width: "800px"
};

var submitButtonStyle = {
    padding: "20px 20px"
};

var outputMatrixStyle = {
    padding: "30px 30px",
    float: "left"
};


var outputMatrixGenerator = React.createClass({
    getInitialState() { return { forceupdate: 1 } },
    render() {
        var output = [];
        for(var matrixName in this.props.outputDict) {
            if(this.props.outputDict.hasOwnProperty(matrixName)){
                var sanitized_matrix = commonFuncs.sanitizeResultMatrix(this.props.outputDict[matrixName]);
                output.push(<div key={ matrixName.toString() }>{ matrixName } </div>);
                output.push(<MatrixModule key={matrixName.toString()+"Value"} columns={ sanitized_matrix } readonly={true} resize="none" />);
            }
        }
        return (
            <div id="output_matrices" key={ this.state.forceupdate } >
            {output}
            </div>
        )
    }
});

var DataMatrixInput = React.createClass({
    render() {
        if (this.props.initial_matrix_value === null) { return (<div>Loading...</div>); }
        return (
            <div id="right_pane" style={ rightPaneStyle } >  
                <div>Enter the matrix to solve</div> 
                <MatrixModule columns={ this.props.initial_matrix_value } ref={(ref) => this.props.updateInputMatrix(ref)} /> 
            </div>
        );
    }
});


var RowColumnSelector = React.createClass({
  getInitialState() {
    return { dec_type: 'Select type.', rows: 3, columns: 3, matrix_list: [[1,1,1],[1,1,1],[1,1,1]], forceupdate: 1 };
  },

  setRowCallBack: function(value){
    var new_matrix = commonFuncs.createMatrixFromDimensions(value, this.state.columns, this.state.matrix_list);
    var new_index = this.state.forceupdate + 1;
    this.setState({ rows: value, matrix_list: new_matrix, forceupdate: new_index });
  },

  setColumnCallBack: function(value){
    var new_matrix = commonFuncs.createMatrixFromDimensions(this.state.rows, value, this.state.matrix_list);
    var new_index = this.state.forceupdate + 1;
    this.setState({ columns: value, matrix_list: new_matrix, forceupdate: new_index });
  },

  setInputMatrix: function(input_matrix) {
    this._matrix_comp = input_matrix;
  },

  handleSubmit: function() {
    $.ajax({
    url: '/submit',
    type: 'POST',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({ dec_type: this.state.dec_type, data: commonFuncs.getCellDataFromMatrix(this._matrix_comp) }),
    success: function(msg) {
        ReactDOM.unmountComponentAtNode(document.getElementById("output_matrix"));
        if (msg.lastIndexOf("{", 0) === 0) {
            ReactDOM.render(React.createElement(outputMatrixGenerator, { outputDict: JSON.parse(msg) })
                          , document.getElementById("output_matrix"));
        }
        else {
            ReactDOM.render(<div>{msg}</div>
                          , document.getElementById("output_matrix"));
        }
    }.bind(this)
    });
  },
  render() {
     return (
         <div id="inputs" style={ inputsStyle } >
            <div id="left_pane" style={ leftPaneStyle } >
                <div>Select decomposition type.</div> 
                <DropdownList data={this.props.decompList} value={this.state.dec_type} onChange={ value => this.setState({ dec_type: value }) } />
                <div>Select number of rows</div> 
                <NumberPicker value={this.state.rows} min={2} max={10} step={1} onChange={this.setRowCallBack} ></NumberPicker> 
                <div>Select number of columns</div> 
                <NumberPicker value={this.state.columns} min={2} max={10} step={1} onChange={this.setColumnCallBack} ></NumberPicker>
                <div style={ submitButtonStyle }> 
                    <button type="submit" onClick={this.handleSubmit}>Calculate decomposition</button> 
                </div>
            </div> 
            <DataMatrixInput updateInputMatrix={this.setInputMatrix} initial_matrix_value={this.state.matrix_list} key={this.state.forceupdate} />
         </div>);
    }
});


module.exports = React.createClass({
    getInitialState() {
        return { decompList: [] };
    },
    componentDidMount() {
        this.serverRequest = $.get("/getAvailableDecompositions", function (result) {
            result = result.replace(/'/g, '"');
            var lastGist = JSON.parse(result);
            this.setState({ decompList: lastGist });
        }.bind(this));
    },
    componentWillUnmount() {
        this.serverRequest.abort();
    },
    render() {
        if (this.state.decompList.length == 0) {
            return (<div id="matrix_page_div"></div>);
        }
        else {
            return (<div id="matrix_page_div">
                        <RowColumnSelector decompList={this.state.decompList}/>
                        <div id="output_matrix" style={ outputMatrixStyle } ></div>
                    </div>);
        }
    }
});