'use strict';

var React = require("react");
var ReactDOM = require("react-dom");


module.exports = React.createClass({
    render() {
        return (<div id="index_id">
                <div>A playground for all kinds of linear-algebra related applications.</div>
                <div>This is an effort to gather various sparse and relatively less-known frameworks and libraries that revolve around linear algebra, and make a showcase of the cool things that were made possible with them.</div>
                <div>The areas of focus are lighting/shading algorithms, image processing, fractal generation and linear systems.</div>
                </div>);
    }
});