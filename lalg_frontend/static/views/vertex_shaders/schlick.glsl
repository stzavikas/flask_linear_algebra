module.exports =
`
uniform vec4 ViewerPos;
uniform vec3 LightPos;

varying vec4 ViewerVec;
varying vec3 LightVec;
varying vec3 vNormal;

void main(void) {

   ViewerVec = normalize(ViewerPos - vec4(position, 1.0));

   LightVec = vec3(LightPos - position);
   vNormal = normal;

   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`