module.exports =
`

varying vec3 vNormal;
varying mat4 MVMatrix;

void main(void) {
    MVMatrix = modelViewMatrix;
    vNormal = normalize(normalMatrix * normal);
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`