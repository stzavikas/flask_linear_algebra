module.exports =
`
attribute vec3 rm_Binormal;
attribute vec3 rm_Tangent;

uniform vec4 ViewerPos;
uniform vec4 LightPos;
uniform float Roughx;
uniform float Roughy;

varying vec3 LightVec,vNormal,halfvec,X,Y,Refl;

void main(void) {
   vNormal = normal;
   vec4 pos = normalize(modelViewMatrix * ViewerPos - modelViewMatrix * vec4(position, 1.0));
   LightVec = normalize(vec3(LightPos) - position);
//   LightVec = normalize(vec3(4.0,4.0,4.0));

   halfvec = normalize(LightVec + vec3(pos));

   Refl = normalize(reflect(vec3(modelViewMatrix * vec4(position, 1.0)),vNormal));
   X = normalize(normalMatrix * rm_Tangent);
   Y = normalize(normalMatrix * rm_Binormal);
   //X=normalize(cross(vNormal,vec3(0.0,0.0,1.0)));
   //Y=normalize(cross(vNormal,X));

   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`