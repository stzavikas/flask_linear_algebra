module.exports =
`
uniform vec3 LightPosition;
uniform vec3 Sky;
uniform vec3 Ground;
varying vec4 VertColor;


void main(void) {
   vec3 vPosition = vec3(modelViewMatrix * vec4(position, 1.0));
   vec3 norm = normalize(normalMatrix * normal);
   vec3 lightvec = normalize(LightPosition - vPosition);
   float a = 0.3 + 0.3 * dot(norm,lightvec);
   VertColor = vec4(mix(Ground,Sky,a),1.0);
   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`