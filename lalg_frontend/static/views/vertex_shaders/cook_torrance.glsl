module.exports =
`
uniform vec4 ViewerPos;
uniform vec3 LightPos;
uniform vec3 Rough;
uniform vec4 Diffuse;

varying vec4 finalColor;

void main(void) {

   vec4 pos = normalize(ViewerPos - vec4(position, 1.0));
   vec3 LightVec = normalize(LightPos - position);
   vec3 HalfVec = normalize(LightVec + pos.xyz);
   float NdotH = max(0.0, dot(normal, HalfVec));
   //Distribution term D
   float alpha = acos(NdotH);
   float D = Rough.x * exp(-(pow(alpha/Rough.y, 2.0)));

   //Geometric term G
   float NdotV = dot(normal, vec3(pos));
   float VdotH = dot(HalfVec, vec3(pos));
   float NdotL = dot(LightVec, normal);
   float G1 = 2.0 * NdotH*NdotV/NdotH;
   float G2 = 2.0 * NdotH * NdotL / NdotH;
   float G = min(1.0, max(0.0, min(G1, G2)));

   //Fresnel term F
   float F = Rough.z + (1.0 - Rough.z) * pow(1.0 - NdotL, 5.0);

   finalColor = Diffuse * F * G * D / (NdotL * NdotV);

   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`