module.exports =
`
uniform vec3 LightPos;
uniform vec4 ViewerPos;
uniform vec4 Diffuse;
uniform float power;

varying vec4 vColor;

void main(void) {
   vec4 pos = normalize(ViewerPos - vec4(position, 1.0));
   vec3 LightVec = normalize(LightPos - position);
   vec3 norm = normalize(normalMatrix * normal);
   float VdotN = dot(vec3(pos), vec3(norm));
   float LdotN = dot(LightVec, vec3(norm));
   float Irradiance = max(0.0, LdotN);
   vColor = Diffuse * pow(VdotN * LdotN, power) * Irradiance;
   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`