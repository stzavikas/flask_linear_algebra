module.exports =
`
uniform vec4 LightPos;
uniform vec4 ViewerPos;
uniform float Roughness;
uniform vec3 MaterialColor;
varying vec4 Diffuse;

void main(void) {

   vec3 V2E = normalize(vec3(ViewerPos) - position);
   vec3 LightVec = normalize(vec3(LightPos) - position);

   vec3 tnorm = normalize(normalMatrix * normal);
   float VdotN = dot(V2E,tnorm);
   float LdotN = dot(LightVec,tnorm);
   float Irradiance = max(0.0, LdotN);

   float AngleVN = acos(VdotN);
   float AngleLN = acos(LdotN);

   float AD = max(0.0,dot(normalize(V2E - tnorm * VdotN),
                          normalize(LightVec - tnorm * LdotN)));

   float alpha = max(AngleVN, AngleLN);
   float beta = min(AngleVN, AngleLN);

   float A = 1.0 - (0.5 * Roughness) / (Roughness + 0.33);
   float B = (0.45 * Roughness) / (Roughness + 0.09);

   Diffuse = vec4(MaterialColor, 0.0) * (A + B*AD*sin(alpha)*tan(beta)) * Irradiance;

   gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`