'use strict';

var React = require('react');
var ReactDOM = require('react-dom');
var THREE = require('three');
var OBJLoader = require('three-obj-loader');
OBJLoader(THREE);
var DropdownList = require("react-widgets/lib/DropdownList.js");

var hemisphereVertexVSh = require("./vertex_shaders/hemisphere_lighting_vertex.glsl");
var hemisphereVertexFSh = require("./pixel_shaders/hemisphere_lighting_vertex.glsl");
var hemisphereFragmentVSh = require("./vertex_shaders/hemisphere_lighting_fragment.glsl");
var hemisphereFragmentFSh = require("./pixel_shaders/hemisphere_lighting_fragment.glsl");
var minnaertVSh = require("./vertex_shaders/minnaert.glsl");
var minnaertFSh = require("./pixel_shaders/minnaert.glsl");
var schlickVSh = require("./vertex_shaders/schlick.glsl");
var schlickFSh = require("./pixel_shaders/schlick.glsl");
var cookVSh = require("./vertex_shaders/cook_torrance.glsl");
var cookFSh = require("./pixel_shaders/cook_torrance.glsl");
var orenVSh = require("./vertex_shaders/oren.glsl");
var orenFSh = require("./pixel_shaders/oren.glsl");
var wardVSh = require("./vertex_shaders/ward_anisotropic.glsl");
var wardFSh = require("./pixel_shaders/ward_anisotropic.glsl");

class ShaderObject extends React.Component {
    _onAnimate() {
        this.setState({
            meshRotation: new THREE.Euler(
                this.state.meshRotation.x + 0.01,
                this.state.meshRotation.y + 0.01,
                0)
        });
    }
    initRendering() {
        this.mainScene = new THREE.Scene();

        this.sceneRenderer = new THREE.WebGLRenderer();
        this.sceneRenderer.setSize(800, 800);
        document.getElementById('threeCanvas').appendChild(this.sceneRenderer.domElement);

        this.sceneCamera = new THREE.PerspectiveCamera(75, 800 / 800, 0.1, 1000);
        this.sceneCamera.position.set(-2, 2, 2);
        this.sceneCamera.lookAt(new THREE.Vector3(0,0,0));
        this.mainScene.add(this.sceneCamera);

        var oLoader = new THREE.OBJLoader();
        var material = new THREE.ShaderMaterial({uniforms: this.props.uniforms,
                                                 vertexShader: this.props.vertexShader,
                                                 fragmentShader: this.props.fragmentShader});
        oLoader.load('../static/views/wt_teapot.obj', function(object, materials) {
            object.traverse( function(child) {
                var loadedObject = new THREE.Mesh(object.children[0].geometry, material);
                loadedObject.position.set(0.,0.,0.);
                this.mainScene.add(loadedObject);
                this.setState({loadedObject: loadedObject});
            }.bind(this));
        }.bind(this));
    }
    animateRendering() {
        requestAnimationFrame(this.animateRendering.bind(this));
        this.sceneRenderer.render(this.mainScene, this.sceneCamera);
    }
    constructor(props, context) {
        super(props, context);
        this.state = {loadedObject: null,
                      meshRotation: new THREE.Euler(),
                      meshPosition: new THREE.Vector3(0,0,0)
        };
    }
    componentDidMount() {
        this.initRendering();
        this.animateRendering();
    }
    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(document.getElementById("threeCanvas"));
    }
    componentWillReceiveProps(nextProps) {
        this.mainScene.remove(this.state.loadedObject);
        this.state.loadedObject.material = new THREE.ShaderMaterial({uniforms: nextProps.uniforms,
                                                                     vertexShader: nextProps.vertexShader,
                                                                     fragmentShader: nextProps.fragmentShader});

        this.mainScene.add(this.state.loadedObject);
    }
    render() {
        return (<div />);
    }
}


class MaterialShaders extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            selected_shader_name: "Hemisphere Lighting Vertex"
        };
        this.materialShaderDict = {
            "Hemisphere Lighting Vertex": [hemisphereVertexVSh, hemisphereVertexFSh, {
                "LightPosition": { value: new THREE.Vector3(3000, -2000, 2000)},
                "Sky": { value: new THREE.Vector3(0.2, 0.4, 0.2)},
                "Ground": { value: new THREE.Vector3(0.08, 0.08, 0.08)}
            }],
            "Hemisphere Lighting Fragment": [hemisphereFragmentVSh, hemisphereFragmentFSh, {
                "LightPosition": { value: new THREE.Vector3(3000, -2000, 2000)},
                "Sky": { value: new THREE.Vector3(0.2, 0.4, 0.2)},
                "Ground": { value: new THREE.Vector3(0.08, 0.08, 0.08)}
            }],
            "Minnaert - Velvet": [minnaertVSh, minnaertFSh, {
                "LightPos": { value: new THREE.Vector3(300, 200, 200)},
                "ViewerPos": { value: new THREE.Vector4(3000, -2000, 2000, 1.0)},
                "Diffuse": { value: new THREE.Vector4(0.52, 0.91, 0.52, 1.0)},
                "power": { value: 1.0 }
            }],
            "Schlick - Optimized Phong": [schlickVSh, schlickFSh, {
                "LightPos": { value: new THREE.Vector3(300, 200, 200)},
                "ViewerPos": { value: new THREE.Vector4(3000, -2000, 2000, 1.0)},
                "Diffuse": { value: new THREE.Vector4(0.2, 0.6, 0.2, 1.0)},
                "Specular": { value: new THREE.Vector4(0.4, 0.4, 0.4, 1.0)},
                "Shininess": { value: 18.0 }
            }],
            "Cook Torrance - Metallic": [cookVSh, cookFSh, {
                "LightPos": { value: new THREE.Vector3(-300, 400, 200)},
                "ViewerPos": { value: new THREE.Vector4(3000, -2000, 2000, 1.0)},
                "Diffuse": { value: new THREE.Vector4(0.2, 0.6, 0.2, 1.0)},
                "Rough": { value: new THREE.Vector3(0.6, 0.5, 1.0)}
            }],
            "Oren Nayar - Dust": [orenVSh, orenFSh, {
                "LightPos": { value: new THREE.Vector3(3000, -4000, 2000)},
                "ViewerPos": { value: new THREE.Vector4(3000, -2000, 2000, 1.0)},
                "Roughness": { value: 0.98 },
                "MaterialColor": { value: new THREE.Vector3(0.2, 0.6, 0.2)}
            }]//,
//TODO: Fix anisotropic Ward
//            "Anisotropic Ward": [wardVSh, wardFSh, {
//                "LightPos": { value: new THREE.Vector3(-300, 400, 200)},
//                "ViewerPos": { value: new THREE.Vector4(3000, -2000, 2000, 1.0)},
//                "Roughx": { value: 0.285 },
//                "Roughy": { value: 0.285 },
//                "Diffuse": { value: new THREE.Vector3(0.2, 0.6, 0.2)},
//                "Specular": { value: new THREE.Vector3(0.7, 0.7, 0.7)},
//                "R" : { value: new THREE.Vector2(-0.66, 1.35)}
//            }],
        };
    }
    dropDownSelectCallback(value) {
        this.setState({selected_shader_name: value});
    }
    componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(document.getElementById("threeCanvas"));
    }
    render() {
        return (
            <div id="material_page">
                <DropdownList data={Object.keys(this.materialShaderDict)}
                              value={this.state.selected_shader_name}
                              onChange={this.dropDownSelectCallback.bind(this)} />
                <ShaderObject vertexShader={this.materialShaderDict[this.state.selected_shader_name][0]}
                              fragmentShader={this.materialShaderDict[this.state.selected_shader_name][1]}
                              uniforms={this.materialShaderDict[this.state.selected_shader_name][2]} />
            </div>
        );
    }
}


var materialPageStyle = {
    float: "left",
    width: "800px"
};


module.exports = React.createClass({
    render() {
        return (<div style={ materialPageStyle } >
                    <div>This is a display of various material GLSL shaders. Each shader displays a lighting algorithm that tries to simulate a material type.</div>
                    <br />
                    <div>
                        <MaterialShaders />
                        <div id="threeCanvas" />
                    </div>
                </div>
        );
    }
})
