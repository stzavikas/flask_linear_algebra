module.exports =
`
uniform vec3 LightPosition;
uniform vec3 Sky;
uniform vec3 Ground;

varying vec3 vNormal;
varying mat4 MVMatrix;

void main(void)
{
   vec3 fragpos = vec3(MVMatrix * (gl_FragCoord));
   vec3 lightvec = normalize(LightPosition - fragpos);
   float a = 0.3 + 0.3 * dot(vNormal,lightvec);
   vec4 VertColor = vec4(mix(Ground,Sky,a),0.0);
   gl_FragColor = VertColor;
}
`