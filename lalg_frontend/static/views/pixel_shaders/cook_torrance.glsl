module.exports =
`
varying vec4 finalColor;

void main(void) {
   gl_FragColor = finalColor;
}
`