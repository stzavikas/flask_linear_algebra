module.exports =
`
        precision highp float;
        varying vec2 uv;
        uniform float convolutionKernel[9];
        uniform bool is_rgb;
        uniform sampler2D image;
        uniform float textureSizeX;
        uniform float textureSizeY;


        vec4 get_pixel(in vec2 coords, in float dx, in float dy) {
           return texture2D(image, coords + vec2(dx, dy));
        }

        float Convolve(in float[9] kernel, in float[9] matrix,
                       in float denom, in float offset) {
           float res = 0.0;
           for (int i=0; i<9; i++) {
              res += kernel[i]*matrix[i];
           }
           return clamp(res/denom + offset,0.0,1.0);
        }

        vec3 GetDataAndConvolve() {

           float dxtex = 1.0 / textureSizeX;
           float dytex = 1.0 / textureSizeY;
           float matr[9];
           float matg[9];
           float matb[9];
           for (int col=0; col<3; col++) {
             if (col==0) {
                matr[0] = float(get_pixel(uv, 0.*dxtex, 0.*dytex)[0]);
                matr[1] = float(get_pixel(uv, 0.*dxtex, 1.*dytex)[0]);
                matr[2] = float(get_pixel(uv, 0.*dxtex, 2.*dytex)[0]);
                matr[3] = float(get_pixel(uv, 1.*dxtex, 0.*dytex)[0]);
                matr[4] = float(get_pixel(uv, 1.*dxtex, 1.*dytex)[0]);
                matr[5] = float(get_pixel(uv, 1.*dxtex, 2.*dytex)[0]);
                matr[6] = float(get_pixel(uv, 2.*dxtex, 0.*dytex)[0]);
                matr[7] = float(get_pixel(uv, 2.*dxtex, 1.*dytex)[0]);
                matr[8] = float(get_pixel(uv, 2.*dxtex, 2.*dytex)[0]);
             }
             else if (col==1) {
                matg[0] = float(get_pixel(uv, 0.*dxtex, 0.*dytex)[1]);
                matg[1] = float(get_pixel(uv, 0.*dxtex, 1.*dytex)[1]);
                matg[2] = float(get_pixel(uv, 0.*dxtex, 2.*dytex)[1]);
                matg[3] = float(get_pixel(uv, 1.*dxtex, 0.*dytex)[1]);
                matg[4] = float(get_pixel(uv, 1.*dxtex, 1.*dytex)[1]);
                matg[5] = float(get_pixel(uv, 1.*dxtex, 2.*dytex)[1]);
                matg[6] = float(get_pixel(uv, 2.*dxtex, 0.*dytex)[1]);
                matg[7] = float(get_pixel(uv, 2.*dxtex, 1.*dytex)[1]);
                matg[8] = float(get_pixel(uv, 2.*dxtex, 2.*dytex)[1]);
             }
             else if (col==2) {
                matb[0] = float(get_pixel(uv, 0.*dxtex, 0.*dytex)[2]);
                matb[1] = float(get_pixel(uv, 0.*dxtex, 1.*dytex)[2]);
                matb[2] = float(get_pixel(uv, 0.*dxtex, 2.*dytex)[2]);
                matb[3] = float(get_pixel(uv, 1.*dxtex, 0.*dytex)[2]);
                matb[4] = float(get_pixel(uv, 1.*dxtex, 1.*dytex)[2]);
                matb[5] = float(get_pixel(uv, 1.*dxtex, 2.*dytex)[2]);
                matb[6] = float(get_pixel(uv, 2.*dxtex, 0.*dytex)[2]);
                matb[7] = float(get_pixel(uv, 2.*dxtex, 1.*dytex)[2]);
                matb[8] = float(get_pixel(uv, 2.*dxtex, 2.*dytex)[2]);
             }
           }
           float mata[9];
           for (int i=0; i<9; i++) {
             mata[i] = (matr[i] + matb[i] + matb[i])/3.0;
           }
           if ( !is_rgb ) {
               float intensity = Convolve(convolutionKernel, mata, 1.0, 0.);
               return vec3(intensity, intensity, intensity);
           }
           else {
               return vec3(Convolve(convolutionKernel, matr, 1., 0.),
                           Convolve(convolutionKernel, matg, 1., 0.),
                           Convolve(convolutionKernel, matb, 1., 0.));
           }
        }

        void main () {
           vec3 result = GetDataAndConvolve();
           gl_FragColor = vec4(result.x, result.y, result.z, 1.0);
        }
`