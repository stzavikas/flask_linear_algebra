module.exports =
`
    precision highp float;
    varying vec2 uv;
    uniform float uniform1;

    int get_iteration_of_divergence(vec2 c, vec2 current_coords) {
        vec2 z = current_coords;
        // Values between -10 and 10, produce different images
        float cr = 1.0;
		float ci = 1.0;
        for (int k=0; k<200; k++) {
            float a = z.x;
            float b = z.y;
            z.x = a / cos(b) + cr;
            z.y = b / sin(a) + ci;
            if (pow(z.x, 2.0) + pow(z.y, 2.0) > uniform1) {
                return k;
            }
        }
        return 0;
    }

    void main() {
        vec2 c = vec2( 0.285, 0.01 );
        int iteration_result=get_iteration_of_divergence(c, uv);
        if (iteration_result == 0) {
            gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        }
        else {
            gl_FragColor = vec4(float(iteration_result)/10.0, float(iteration_result)/10.0, float(iteration_result)/10.0, 1.0);
        }
    }

`