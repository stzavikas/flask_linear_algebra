module.exports =
`

varying vec4 VertColor;

void main(void)
{
   gl_FragColor = VertColor;
}

`