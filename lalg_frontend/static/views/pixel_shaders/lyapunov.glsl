module.exports =
`
        precision highp float;
        varying vec2 uv;
        uniform float uniform1;

        void main() {
            const int iterations = 1000;
            float a = uv.x + 2.9; //a, b should be in the [0,4] range
            float b = uv.y + 2.9;
            float x = 0.5;
            float sum = 0.0;
            float r = b; //Zircon city sequence, bbbbbaaaaa
            for (int i=0; i < iterations; i++) {
                if (i > int(uniform1)) { break; }
                float x_next = r * x * (1.0-x);
                int modulo = i - 10 * int(floor(float(i) / 10.0));
                float r_next = (modulo > 5)?a:b;
                sum += log(abs(r_next * (1.0 - 2.0 * x_next)));
                x = x_next;
                r = r_next;
            }
            sum = sum / float(iterations);

            if (sum < 0.0) {
                gl_FragColor = vec4(abs(sum), abs(sum), 0.0, 1.0);
            }
            else {
                gl_FragColor = vec4(0.0, 0.0, sum*2.0, 1.0);
            }
        }
`
