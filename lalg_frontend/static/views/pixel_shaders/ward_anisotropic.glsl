module.exports =
`
uniform float Roughx;
uniform float Roughy;
uniform vec3 Diffuse;
uniform vec4 Specular;
uniform vec2 R;

varying vec3 LightVec,vNormal,halfvec,X,Y,Refl;

void main(void) {

   float firstt = 1.0 / (sqrt((dot(vNormal, LightVec)) * (dot(vNormal, Refl))));
   float secondt = 1.0 / (6.28*Roughx*Roughy);

   float HdotX = dot(halfvec,X);
   float HdotY = dot(halfvec,Y);
   float HdotN = dot(halfvec,vNormal);
   float A = -2.0*((HdotX/Roughx)*(HdotX/Roughx) + (HdotY/Roughy)*(HdotY/Roughy));
   float B = 1.0 + HdotN;
   float thirdt = exp(A/B);
   float brdf = firstt * secondt * thirdt;

   float term=(R.x/3.14159) + R.y * brdf;
   vec3 FinalColor = vec3(((R.x / 3.14159) +
                       R.y * dot(vNormal,LightVec) * term +
                       dot(halfvec, vNormal) * R.y) * Specular) + Diffuse;

   gl_FragColor = vec4(FinalColor,1.0);
}

`