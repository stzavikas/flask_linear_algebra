module.exports =
`
    precision highp float;
    varying vec2 uv;
    uniform float uniform1;

    int get_iteration_of_divergence(vec2 c, vec2 current_coords) {
        vec2 z = current_coords;
        for (int i=0; i<400; i++) {
            float x = pow(z.x, 2.0) - pow(z.y, 2.0) + c.x;
            float y = 2.0 * z.x * z.y + c.y;
            if( (pow(x, 2.0) + pow(y, 2.0) ) > uniform1) {
                return i;
            }
            z.x = x;
            z.y = y;
        }
        return 0;
    }

    void main() {
        vec2 c = vec2( 0.285, 0.01 );
        int iteration_result=get_iteration_of_divergence(c, uv);
        if (iteration_result == 0) {
            gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        }
        else {
            gl_FragColor = vec4(float(iteration_result)/200.0, sin(float(iteration_result)/50.0), float(iteration_result)/200.0, 1.0);
        }
    }
`