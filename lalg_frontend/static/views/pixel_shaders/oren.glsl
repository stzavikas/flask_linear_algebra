module.exports =
`
varying vec4 Diffuse;

void main(void) {
   gl_FragColor = Diffuse;
}
`