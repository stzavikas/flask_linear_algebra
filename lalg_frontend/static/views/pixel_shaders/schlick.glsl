module.exports =
`
uniform vec4 Diffuse;
uniform vec4 Specular;
uniform float Shininess;

varying vec4 ViewerVec;
varying vec3 LightVec;
varying vec3 vNormal;


void main(void)
{
   vec3 nLightVec = normalize(LightVec);
   vec3 reflVector = normalize(2.0 * vNormal * dot(vNormal, nLightVec) - nLightVec);
   float RdotV = max(0.0, dot(reflVector, vec3(normalize(ViewerVec))));
   float schl = RdotV / (Shininess - ((Shininess - 1.0) * RdotV));

   vec4 diffuse = Diffuse * max(0.0,dot(nLightVec,vNormal));
   vec4 specular = Specular * schl;

   vec4 col = diffuse + specular;

   gl_FragColor = col;
}
`