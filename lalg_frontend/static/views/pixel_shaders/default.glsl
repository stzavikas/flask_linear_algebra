module.exports =
`
        precision highp float;
        varying vec2 uv;
        uniform float uniform1;

        void main () {
            gl_FragColor = vec4(uv.x, uv.y, uniform1, 1.0);
        }
`