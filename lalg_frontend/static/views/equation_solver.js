'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var $ = require("jQuery");
var Griddle = require("griddle-react");

var EquationGenerator = React.createClass({
    getInitialState() {
        return { eq_string: this.props.initialValue.toString(), divName: ("MathDiv" + this.props.divId.toString()) }
    },
    onChange(e) {
        var updatedValue = '$$' + e.target.value.toString() + '$$';
        ReactDOM.unmountComponentAtNode(document.getElementById(this.state.divName));
        ReactDOM.render(React.createElement('div', { id: this.state.divName, value: updatedValue })
                          , document.getElementById(this.state.divName));
        this.setState({ eq_string: updatedValue }, ()=> {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.state.divName]);
        });
        this.props.updateFunc(this.props.divId, updatedValue);
    },
    deleteSelf(e) {
        this.props.deleteFunc(this.props.divId);
    },
    render() {
        // Deleting $$ to hide it from the input field, it is automatically embedded.
        var equation = this.state.eq_string.replace(/\$\$+/g, '');
        return (
            <div>
                <input onChange={this.onChange} value={equation} />
                <button type="deleteEquation" onClick={ this.deleteSelf }>Delete Equation</button>
                <div key={this.props.divId} id={this.state.divName}>{ this.state.eq_string }</div>
            </div>
        );
    }
});


var EquationSolveResult = React.createClass({
    render() {
        var results = this.props.resultData;
        if (results.lastIndexOf("{", 0) === 0) {
            var resultDict = JSON.parse(results);
            var resultArray = [];
            for(var resultKey in resultDict) {
                if(resultDict.hasOwnProperty(resultKey)){
                    resultArray.push({'Name': resultKey, 'Value': resultDict[resultKey]});
                }
            }
            return(<div><Griddle results={ resultArray } /></div>);
        }
        else {
            return(<div>{this.props.resultData}</div>);
        }
    }
});


var EquationSet = React.createClass({
    getInitialState() {
        return { eq_list: [""], forceupdate: 1 }
    },
    handleEquationAdd(){
        var new_eq_list = this.state.eq_list;
        new_eq_list.push("");
        this.setState({eq_list: new_eq_list});
    },
    solveEquation() {
        var dataToSend = []
        for (var id=0; id<this.state.eq_list.length; ++id) {
            dataToSend.push(this.state.eq_list[id].replace(/\$\$+/g, ''));
        }
        $.ajax({
            url: '/solveEquation',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ equations: dataToSend }),
            success: function(msg) {
                ReactDOM.unmountComponentAtNode(document.getElementById('equation_output'));
                ReactDOM.render(React.createElement(EquationSolveResult, { resultData: msg })
                              , document.getElementById('equation_output'));
            }.bind(this)
        });
    },
    updateEquations(id, value) {
        var new_eq_list = this.state.eq_list;
        new_eq_list[id] = value;
        this.setState({ eq_list: new_eq_list }, () => { MathJax.Hub.Queue(["Typeset",MathJax.Hub]); });
    },
    deleteEquation(id) {
        var new_eq_list = this.state.eq_list;
        new_eq_list.splice(id,1);
        this.setState({eq_list: new_eq_list, forceupdate: this.state.forceupdate+this.state.eq_list.length+1}, () => { MathJax.Hub.Queue(["Typeset",MathJax.Hub]); });
    },
    render() {
        var output = [];
        for (var i=0; i<this.state.eq_list.length; ++i) {
            output.push(<EquationGenerator key={this.state.forceupdate+i} divId={i} initialValue={this.state.eq_list[i]} updateFunc={ this.updateEquations } deleteFunc={ this.deleteEquation } />);
        }
        output.push(<button type="addEquation" key="add_button" onClick={this.handleEquationAdd}>Add Equation</button>);
        output.push(<button type="solveEquation" key="delete_button" onClick={this.solveEquation}>Solve Equation System</button>);
        return (<div key="equation_set">{output}</div>);
    }
});

var solverPageStyle = {
    float: "left",
    width: "400px"
};

module.exports =  React.createClass({
    render() {
        return (<div style={ solverPageStyle } >
                <div>Linear solver. Supports mathjax format.</div>
                <br />
                <EquationSet />
                <div id='equation_output'></div>
                </div>);
    }
});