'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var $ = require("jQuery");
const GL = require("gl-react");
const { Surface } = require("gl-react-dom"); // in React DOM context
var DropdownList = require("react-widgets/lib/DropdownList.js");
var ReactSlider = require("rc-slider");
var JuliaShader = require("./pixel_shaders/julia_set.glsl");
var MandelbrotShader = require("./pixel_shaders/mandelbrot_set.glsl");
var LyapunovShader = require("./pixel_shaders/lyapunov.glsl");
var ThornShader = require("./pixel_shaders/thorn_fractal.glsl");
var TestShader = require("./pixel_shaders/default.glsl");


var ShaderComponent = GL.createComponent(
  ({ shaderObject, uniform1, image, textureSizeX, textureSizeY}) =>
  <GL.Node
    shader={ shaderObject[0].fragment_shader }
    uniforms={ {uniform1} }
  />,
  { displayName: "HelloGL" });


var fractalSelectorStyle = {
    float: "left",
    width: "350px",
    padding: "20px 20px"
};

var shaderFractalStyle = {
    float: "center",
    width: "800px"
};


var ShaderClass = React.createClass({
    getInitialState() {
        return { selected_shader: Object.keys(this.props.shaderArray)[0], uniform_value: Object.values(this.props.shaderArray)[0][2] };
    },
    setUniformCallback(value) {
        var min_value = this.props.shaderArray[this.state.selected_shader][3];
        var max_value = this.props.shaderArray[this.state.selected_shader][4];
        this.setState({uniform_value: (min_value + (value/100.0)*(max_value-min_value))});
    },
    dropDownSelectCallback(value) {
        this.setState({selected_shader: value, uniform_value: this.props.shaderArray[value][2]});
    },
    render() {
        return (<div id="fractalSelectors">
                    <div id="dropdownFractal" style={ fractalSelectorStyle }>
                        <DropdownList data={Object.keys(this.props.shaderArray)} value={this.state.selected_shader} onChange={this.dropDownSelectCallback} />
                    </div>
                    <div id="sliderFractal" style={ fractalSelectorStyle }>
                        <div>{this.props.shaderArray[this.state.selected_shader][1]}</div>
                        <ReactSlider defaultValue={50} onChange={this.setUniformCallback} />
                    </div>
                <div>
                    <div id="shaderFractal" style={ shaderFractalStyle } >
                        <Surface width={800} height={600}>
                            <ShaderComponent shaderObject={this.props.shaderArray[this.state.selected_shader]}
                                             uniform1={this.state.uniform_value} />
                        </Surface>
                    </div>
                </div>
                <div id='shader_output'></div>
                </div>);
    }
});

var JuliaSet = GL.Shaders.create({ fragment_shader: { frag: JuliaShader } });
var DefaultShader = GL.Shaders.create({ fragment_shader: { frag: TestShader } });
var MandelbrotSet = GL.Shaders.create({ fragment_shader: { frag: MandelbrotShader } });
var Lyapunov = GL.Shaders.create({ fragment_shader: { frag: LyapunovShader } });
var Thorn = GL.Shaders.create({ fragment_shader: { frag: ThornShader } });


var fractalsPageStyle = {
    float: "left",
    width: "800px"
};

module.exports = React.createClass({
    getInitialState() {
        return { availableShaders: {
            "Lyapunov" : [Lyapunov, "Iteration Count", 400, 300, 500],
            "Mandelbrot Set": [MandelbrotSet, "Divergence Threshold", 4.0, 1.0, 7.0],
            "Julia Set" : [JuliaSet, "Divergence Threshold", 4.0, 1.0, 50.0],
            "Thorn": [Thorn, "Divergence Threshold", 10000, 1000, 19000]
//            "Default Shader" : [DefaultShader, "blue_intensity", 0.5, 0.1, 0.9]
        }};
    },
    render() {
        return (<div style={ fractalsPageStyle }>
                    <div>A collection of 2D fractals implemented in GLSL.</div>
                    <br />
                    <ShaderClass shaderArray={ this.state.availableShaders } />
                </div>
        );
    }
});
