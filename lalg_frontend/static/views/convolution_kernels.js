'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var $ = require("jQuery");
const GL = require("gl-react");
const { Surface } = require("gl-react-dom"); // in React DOM context
var DropdownList = require("react-widgets/lib/DropdownList.js");
var ReactSlider = require("rc-slider");
var MatrixModule = require("../react_matrix.js");
var commonFuncs = require("../common_functions.js");
var convolution_shader = require("./pixel_shaders/convolution_kernel.glsl");

var ShaderComponent = GL.createComponent(
  ({ convolutionKernel, is_rgb, image, textureSizeX, textureSizeY}) =>
  <GL.Node
    shader={ ConvolutionShader.fragment_shader }
    uniforms={ { convolutionKernel, is_rgb, image, textureSizeX, textureSizeY} }
  />,
  { displayName: "HelloGL" });


var leftSliderStyle = {
    float: "left",
    width: "200px",
};

var rightMatrixStyle = {
    float: "left",
    padding: "0px 150px"
};

var convElementGroupStyle = {
    float: "center",
    width: "800px"
};

var convolutionShaderStyle = {
    float: "center",
    width: "800px",
    padding: "130px 0px"
};

var ShaderClass = React.createClass({
    getInitialState() {
        return { selected_shader: Object.keys(this.props.shaderArray)[0],
                 selected_matrix: commonFuncs.convertArrayToMatrix(Object.values(this.props.shaderArray)[0][0]),
                 selected_array: Object.values(this.props.shaderArray)[0][0],
                 matrixCounter: 1,
                 enabled_editing: false };
    },
    setEditableMatrix(value) {
        this._editable_matrix = value;
    },
    dropDownSelectCallback(value) {
        this.setState({selected_shader: value});
        var updated_matrix = commonFuncs.convertArrayToMatrix(this.props.shaderArray[value][0]);
        this.setState({selected_matrix: updated_matrix });
        this.setState({selected_array: this.props.shaderArray[value][0]});
        var new_matrix_counter = this.state.matrixCounter + 1;
        this.setState({ matrixCounter: new_matrix_counter});
    },
    enableEditing(value) {
        var is_disabled = !this.state.enabled_editing;
        this.setState({enabled_editing: is_disabled});
    },
    updateShader(value) {
        var matr = commonFuncs.getCellDataFromMatrix(this._editable_matrix);
        this.setState({ selected_matrix: matr });
        var arr = commonFuncs.convertConvolutionMatrixToArray(matr);
        this.setState({ selected_array: arr });
        var new_matrix_counter = this.state.matrixCounter + 1;
        this.setState({ matrixCounter: new_matrix_counter});
        this.forceUpdate();
    },
    enableEditButtonText() {
        if (this.state.enabled_editing) {
            return "Disable Edit";
        }
        else {
            return "Enable Edit";
        }
    },
    render() {
        return (<div>
                    <div id="convolution_header" style={ convElementGroupStyle } >
                        <div id="left_slider" style={ leftSliderStyle }>
                            <DropdownList data={Object.keys(this.props.shaderArray)} disabled={this.state.enabled_editing} value={this.state.selected_shader} onChange={this.dropDownSelectCallback} />
                        </div>
                        <div id="matrix_display" style={ rightMatrixStyle }>
                            <MatrixModule key={"kernelValue"} columns={ this.state.selected_matrix }
                                                              key={this.state.matrixCounter}
                                                              readonly={ !this.state.enabled_editing }
                                                              resize="none"
                                                              ref={(ref) => this.setEditableMatrix(ref)} />
                            <br />
                            <button onClick={this.enableEditing}>{ this.enableEditButtonText() }</button>
                            <button onClick={this.updateShader} disabled={!this.state.enabled_editing}>Update Shader</button>
                        </div>
                    </div>
                    <div id="shader_display" style={ convolutionShaderStyle } >
                        <Surface width={800} height={600}>
                            <ShaderComponent convolutionKernel={this.state.selected_array}
                                             is_rgb={this.props.shaderArray[this.state.selected_shader][1]}
                                             image="/static/images/amorgos.jpg"
                                             textureSizeX={640.0}
                                             textureSizeY={640.0} />
                        </Surface>
                    </div>
                </div>);
    }
});


var ConvolutionShader = GL.Shaders.create({ fragment_shader: { frag: convolution_shader } });


module.exports = React.createClass({
    getInitialState() {
        return { availableShaders: {
            "Identity" : [ [ 0., 0., 0., 0., 1., 0., 0., 0., 0. ], true],
            "Box Blur" : [ [ 1./9., 1./9., 1./9., 1./9., 1./9., 1./9., 1./9., 1./9., 1./9. ], true],
            "Gaussian Blur" : [ [ 1./16., 2./16., 1./16., 2./16., 4./16., 2./16., 1./16., 2./16., 1./16. ], true],
            "Emboss Grayscale" : [ [ -2., -1., 0., -1., 1., 1., 0., 1., 2. ], false ],
            "Emboss Colored" : [ [ -2., -1., 0., -1., 1., 1., 0., 1., 2. ], true ],
            "Sharpness" : [ [ -1., -1., -1., -1., 9., -1., -1., -1., -1. ], true],
            "Horizontal Gradient" : [ [ -1., -1., -1., 0., 0., 0., 1., 1., 1. ], false],
            "Vertical Gradient" : [ [ -1., 0., 1., -1., 0., 1., -1., 0., 1. ], false],
            "Laplacian Edge detection" : [ [ 0., -1., 0., -1., 4., -1., 0., -1., 0. ], false ],
            "Edge detection 2" : [ [ 1., 0., -1., 0., 0., 0.,-1., 0., 1. ], false ],
            "Edge detection 3" : [ [ 0., 1., 0., 1., -4., 1., 0., 1., 0. ], false ],
            "Edge detection 4" : [ [ -1., -1., -1., -1., 8., -1., -1., -1., -1. ], false ],
            "Bottom Sobel" : [ [ -1., -2., -1., 0., 0., 0., 1., 2., 1. ], false ],
            "Top Sobel" : [ [ 1., 2., 1., 0., 0., 0., -1., -2., -1. ], false ],
            "Left Sobel" : [ [ 1., 0., -1., 2., 0., -2., 1., 0., -1. ], false ],
            "Right Sobel" : [ [ -1., 0., 1., -2., 0., 2., -1., 0., 1. ], false ],
            "Roberts" : [ [ 0., 0., 0., 1., -1., 0., 0., 0., 0. ], false ],
            "Prewitt" : [ [ 1., 1., 1., 0., 0., 0., -1., -1., -1. ], false ],
            "Compass" : [ [ 1., 1., -1., 1., -2., -1., 1., 1., -1. ], false ],
            "Kirsch" : [ [ 5., -3., -3., 5., 0., -3., 5., -3., -3. ], false ],
            "Frei-Chen" : [ [ 1., 0., -1., 1.41421, 0., -1.41421, 1., 0., -1. ], false ]
        }};
    },
    render() {
        return (<div>
                    <div>Display of different convolution kernel effects on the image. Custom kernels can be applied.</div>
                    <br />
                    <ShaderClass shaderArray={ this.state.availableShaders } />
                </div>
        );
    }
});
