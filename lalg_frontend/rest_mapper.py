from lalg_api.lu import lu_decompose, svd_decompose, cholesky_decompose, polar_decompose, qr_decompose, rq_decompose, schur_decompose, hessenberg_decompose, eigenvalues
from ast import literal_eval


class RestMapper():
    def __init__(self):
        self.rest_endpoints = {'LU': lu_decompose,
                               'SVD': svd_decompose,
                               'Cholesky': cholesky_decompose,
                               'Polar': polar_decompose,
                               'QR': qr_decompose,
                               'RQ': rq_decompose,
                               'Schur': schur_decompose,
                               'Hessenberg': hessenberg_decompose,
                               'Eigenvalues': eigenvalues}

    def get_decomposition_names(self):
        return list(self.rest_endpoints.keys())

    def calculate_decomposition(self, decomp_type, data):
        try:
            a = self.rest_endpoints[decomp_type](literal_eval(str(data)))
            print(a)
            return a
        except Exception as e:
            return "Error: " + str(e)
