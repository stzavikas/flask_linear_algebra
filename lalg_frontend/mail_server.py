from flask_mail import Mail


def setup_mail_server(app):
    app.config['MAIL_SUPPRESS_SEND'] = True
    app.config['MAIL_SERVER'] = 'smtp.example.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USE_SSL'] = True
    app.config['MAIL_USERNAME'] = 'username'
    app.config['MAIL_PASSWORD'] = 'password'
    return Mail(app)