"""from flask.ext.sqlalchemy import SQLAlchemy
import datetime
import hashlib
import config
from flask_login import unicode


db = SQLAlchemy()


class UserRole(db.Model):
    __tablename__ = "user_roles"
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True)
    has_admin_privileges = db.Column(db.Boolean())
    description = db.Column(db.String(255))

    def __init__(self, name, has_admin_privileges=False, description=""):
        self.name = name
        self.has_admin_privileges = has_admin_privileges
        self.description = description


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(20), unique=True , index=True)
    password = db.Column(db.String(255))
    registered_on = db.Column(db.DateTime())
    is_active = False
    user_role_id = db.Column(db.Integer, db.ForeignKey('user_roles.id'))
    user_role = db.relationship('UserRole',
                            backref=db.backref('user_roles', lazy='dynamic'))

    def __init__(self, email, username, password, role):
        self.email = email
        self.username = username
        self.password = password
        self.registered_on = datetime.datetime.utcnow()
        self.user_role = role
        self.is_active = True
        # self.roles = roles

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.is_active

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r %r>' % (self.email, str(self.id))


class DBAccess:
    def __init__(self, app):
        app.config['SQLALCHEMY_DATABASE_URI'] = config.DB_URL
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
        # app.config['SQLALCHEMY_ECHO'] = True
        db = SQLAlchemy(app)
        db.engine.connect()

    def create_user(self, username, email, password):
        # self._generate_schema_and_basic_entries()
        role = UserRole.query.filter_by(name="user").first()
        db.session.add(User(email, username, self.hash_password(password), role))
        db.session.commit()

    def get_user(self, email):
        return User.query.filter_by(email=email).first()

    def get_user_by_username(self, username):
        return User.query.filter_by(username=username).first()

    def get_user_by_id(self, id):
        return User.query.filter_by(id=id).first()

    def delete_database(self):
        db.reflect()
        db.drop_all()
        db.metadata.drop_all(db.engine)

    def hash_password(self, password):
        return hashlib.sha512(password.encode('utf-8')).hexdigest()

    def _generate_schema_and_basic_entries(self):
        db.create_all()
        print(db.metadata.tables.keys())
        admin_role = UserRole(name="root", has_admin_privileges=True, description="I am an admin")
        user_role = UserRole(name="user", has_admin_privileges=False, description="I am a user")
        db.session.add(admin_role)
        db.session.add(user_role)
        db.session.commit()
        admin_user = User(email="skatokafros@hotmail.com", username="root", password=self.hash_password("root"), role=admin_role)
        db.session.add(admin_user)
        db.session.commit()
"""