from flask import Flask, render_template, request, redirect, abort, url_for
from ast import literal_eval
from lalg_frontend.rest_mapper import RestMapper
from lalg_api import register_endpoints
from lalg_api.equation_solver import solve_equation_system
from flask.ext.compress import Compress
# from lalg_frontend.db_model import DBAccess
# from lalg_frontend.mail_server import setup_mail_server
# from flask_login import login_required, LoginManager, login_user, logout_user


COMPRESS_MIMETYPES = ['text/html', 'text/css', 'text/xml', 'application/json', 'application/javascript']
COMPRESS_LEVEL = 6
COMPRESS_MIN_SIZE = 500

# login_manager = LoginManager()
app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

# login_manager.init_app(app)
# login_manager.login_view = "login"

# db = DBAccess(app)
# mail = setup_mail_server(app)
Compress(app)

# @login_manager.user_loader
# def load_user(user_id):
#     return db.get_user_by_id(user_id)


# Create a user to test with
# @app.before_first_request
# def create_user():
#     if db.get_user_by_username(username='root') is None:
#         db._generate_schema_and_basic_entries()

rest_mapper = RestMapper()
register_endpoints(app)


@app.route("/matrixPage")
@app.route("/equationPage")
@app.route("/fractalsPage")
@app.route("/convolutionPage")
@app.route("/materialShaderPage")
@app.route("/matrixPage/")
@app.route("/equationPage/")
@app.route("/fractalsPage/")
@app.route("/convolutionPage/")
@app.route("/materialShaderPage/")
@app.route("/homePage")
@app.route("/homePage/")
# @login_required
def hello():
    return render_template('hello.html')


# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'GET':
#         return render_template('login.html')
#     if request.method == 'POST':
#         user = db.get_user_by_username(username=request.form['username'])
#         print(user)
#         if user is None or db.hash_password(request.form['password']) != user.password:
#             error = 'Invalid Credentials. Please try again.'
#         else:
#             login_user(user)
#             return redirect(url_for('hello'))
#     return render_template('login.html', error=error)

    # next = request.args.get('next')
    # next_is_valid should check if the user has valid
    # permission to access the `next` url
    # implement to avoid redirects
    # if not next_is_valid(next):
    #     return abort(400)


# @app.route("/logout", methods=['POST'])
# @login_required
# def logout():
#     logout_user()
#     return 'OK', 200


@app.route("/submit", methods=['POST'])
def submit():
    try:
        if request.method == 'POST':
            decomp_type = literal_eval(request.data.decode('utf-8'))['dec_type']
            matrix = literal_eval(request.data.decode('utf-8'))['data']
            result = str(rest_mapper.calculate_decomposition(decomp_type, matrix))
            return result.replace("'", '"'), 200
        else:
            return 'Incorrect method', 500
    except Exception as e:
        return 'Not OK', 500
    return 'Error', 404


@app.route("/solveEquation", methods=['POST'])
def solveEquation():
    try:
        if request.method == 'POST':
            equation_input = literal_eval(request.data.decode('utf-8'))['equations']
            return solve_equation_system(equation_input), 200
        else:
            return 'Incorrect method', 500
    except Exception as e:
        print(str(e))
        return 'Not OK', 500
    return 'Error', 404


@app.route("/getAvailableDecompositions", methods=["GET"])
def getDecompositions():
    if request.method == 'GET':
        return str(rest_mapper.get_decomposition_names())
    else:
        return "Unsupported method."


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, debug=True)
