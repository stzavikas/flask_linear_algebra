import unittest
import json
import lalg_api
from ast import literal_eval
from collections import Counter


class DecompositionsTestCase(unittest.TestCase):

    def setUp(self):
        self.app = lalg_api.app.test_client()
        self._data_real = str([[1, 2, 3], [2, 3, 4], [3, 4, 5]])
        self._data_complex = str([[1,-2j],[2j,5]])

    def test_lu_endpoint_works(self):
        result_get = self.app.get('/lu', follow_redirects=True)
        assert "LU Decomposition helper" in str(result_get.data)
        data_to_send={ 'input' : self._data_real}
        result = self.app.post('/lu/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        result2 = literal_eval(str(result.data.decode('utf-8')))
        assert isinstance(result2, dict)

    def test_lu_returns_correct_p_matrix(self):
        data_to_send={ 'input' : self._data_real}
        result = self.app.post('/lu/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        P = matrix_dict['P']
        for row in literal_eval(P):
            counts = Counter(row)
            assert counts[1] == 1
            assert counts[0] == len(row)-1

    def test_lu_returns_correct_U_matrix(self):
        data_to_send={ 'input' : self._data_real}
        result = self.app.post('/lu/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        U = matrix_dict['U']
        diagonal_index = 0
        for row in literal_eval(U):
            for count in range(0, diagonal_index):
                assert row[count] == 0.0
            diagonal_index += 1

    def test_lu_returns_correct_L_matrix(self):
        data_to_send={ 'input' : self._data_real}
        result = self.app.post('/lu/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        L = matrix_dict['L']
        diagonal_index = 1
        for row in literal_eval(L):
            for count in range(diagonal_index, len(row)):
                assert row[count] == 0.0
            diagonal_index += 1

    def test_svd_resolves_matrix_correctly(self):
        result_get = self.app.get('/svd', follow_redirects=True)
        assert "SVD Decomposition helper" in str(result_get.data)
        data_to_send={ 'input' :  self._data_real}
        result = self.app.post('/svd/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'U' in list(matrix_dict.keys())
        assert 's' in list(matrix_dict.keys())
        assert 'Vh' in list(matrix_dict.keys())

    def test_cholesky_resolves_successfully(self):
        result_get = self.app.get('/cholesky', follow_redirects=True)
        assert "Cholesky Decomposition helper" in str(result_get.data)
        data_to_send={'input' : self._data_complex}
        result = self.app.post('/cholesky/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'L' in list(matrix_dict.keys())
        L = matrix_dict['L']
        print(str(L))
        assert literal_eval(str(L)) == [[ 1.+0.j,  0.-2.j],[ 0.+0.j,  1.+0.j]]

    def test_polar_resolves_successfully(self):
        result_get = self.app.get('/polar', follow_redirects=True)
        assert "Polar Decomposition helper" in str(result_get.data)
        data_to_send={'input': str([[1, -1], [2, 4]])}
        result = self.app.post('/polar/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'U' in list(matrix_dict.keys())
        assert 'P' in list(matrix_dict.keys())

    def test_qr_resolves_successfully(self):
        result_get = self.app.get('/qr', follow_redirects=True)
        assert "QR Decomposition helper" in str(result_get.data)
        data_to_send={'input': self._data_real}
        result = self.app.post('/qr/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'Q' in list(matrix_dict.keys())
        assert 'R' in list(matrix_dict.keys())

    def test_rq_resolves_successfully(self):
        result_get = self.app.get('/rq', follow_redirects=True)
        assert "RQ Decomposition helper" in str(result_get.data)
        data_to_send={'input': self._data_real}
        result = self.app.post('/rq/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        print(str(result.data.decode('utf-8')))
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'R' in list(matrix_dict.keys())
        assert 'Q' in list(matrix_dict.keys())

    def test_schur_resolves_successfully(self):
        result_get = self.app.get('/schur', follow_redirects=True)
        assert "Schur Decomposition helper" in str(result_get.data)
        data_to_send={'input': self._data_real}
        result = self.app.post('/schur/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        print(str(result.data.decode('utf-8')))
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'T' in list(matrix_dict.keys())
        assert 'Z' in list(matrix_dict.keys())

    def test_hessenberg_resolves_successfully(self):
        result_get = self.app.get('/hessenberg', follow_redirects=True)
        assert "Hessenberg Decomposition helper" in str(result_get.data)
        data_to_send={'input': str([[2, -3], [-17, 0]])}
        result = self.app.post('/hessenberg/', data=json.dumps(data_to_send), follow_redirects=True, environ_base={'content-type': 'application/json'})
        print(str(result.data.decode('utf-8')))
        matrix_dict = literal_eval(str(result.data.decode('utf-8')))
        assert 'H' in list(matrix_dict.keys())
        assert 'Q' in list(matrix_dict.keys())


if __name__ == '__main__':
    unittest.main()