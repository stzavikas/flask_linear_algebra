import unittest
from lalg_api.equation_solver import convert_root_symbols
from lalg_api.latex.process_latex import process_sympy
from sympy import sympify


class DecompositionsTestCase(unittest.TestCase):

    # def test_convert_root_symbols_operates_for_arbitrary_root(self):
    #     test_expression = 'a+\\sqrt[5]{123+b}=c'
    #     result = process_sympy(test_expression)
    #     assert result == 'a+root((123+b),(5))=c'
    #
    # def test_convert_root_symbols_operates_for_sqrt(self):
    #     test_expression = 'a+\\sqrt{123+b}=c'
    #     result = process_sympy(test_expression)
    #     assert result == 'a+root((123+b),(2))=c'
    #
    # def test_convert_root_symbols_fixes_arbitrary_group(self):
    #     test_expression = 'a+b+{c+123}+{a+{12+b+{34+a}}}=v'
    #     result = process_sympy(test_expression)
    #     assert result == 'a+b+(c+123)+(a+(12+b+(34+a)))=v'

    def test_convert_math_symbols_operates_for_fraction(self):
        test_expression = '\\frac{a+b}{1+a}'
        result = process_sympy(test_expression)
        assert str(result) == '(a + b)/(a + 1)'

    def test_convert_math_symbols_operates_for_multiple_fractions(self):
        test_expression = '\\frac{\\frac{a+b}{a+1}}{b+1}'
        result = process_sympy(test_expression)
        assert str(result) == '((a + b)/(a + 1))/(b + 1)'